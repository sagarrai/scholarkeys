<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Career\CareerRepository;
use Illuminate\Http\Request;
use Validator;

class CareerController extends Controller
{
    public function __construct(CareerRepository $career, BaseController $response){
        $this->career = $career;
        $this->response = $response;
    }

     /**
     * @OA\get(
     *      path="/career",
     *      operationId="getcareerList",
     *      tags={"Career"},
     *      summary="Get all career",
     *      description="Retrive the all career by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function index()
    {
        try {
            $data = $this->career->getAll();
            return $this->response->sendSuccess($data, 'career list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'career list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/career/create",
     *      operationId="StoreCareer",
     *      tags={"Career"},
     *      summary="Create new career",
     *      description="Create new career",
     *
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreCareerRequest")
     *      ),
     *
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CareerModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *
     *      security={ {"bearer": {}} },
     *
     * )
     */
    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                ]);

            if ($validator->fails()) {
                return $this->response->sendError('Validation Error.',['error'=>$validator->errors()],400);
            }

            $data = $this->career->create($request->all());

            return $this->response->sendSuccess($data, 'career created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Career cannot created.'],400);
        }
    }


     /**
     * @OA\get(
     *      path="/career/{id}",
     *      operationId="getcareerDetails",
     *      tags={"Career"},
     *      summary="Get detail of career",
     *      description="Retrive the detail career by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="career id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreCareerRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->career->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'career retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'career id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'career cannot retrived .'],500);
        }
    }

    /**
     *
     * @OA\put(
     *      path="/career/{id}",
     *      operationId="UpdateCareerDetails",
     *      tags={"Career"},
     *      summary="Update career",
     *      description="Update the career by id",
     *
    *       @OA\Parameter(
    *          name="id",
    *          description="career id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    *
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateCareerRequest")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CareerModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function update(Request $request, $id)
    {
        try{
            $validator = Validator::make($request->all(), [
                'title' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);
            }

            $data = $this->career->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'career id not found .'],201);
            }
            $data = $this->career->update($request->all(), $id);
            $data = $this->career->show($id);

            return $this->response->sendSuccess($data, 'career updated success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

    /**
     *
     * @OA\delete(
     *      path="/career/{id}",
     *      operationId="deletecareerDetails",
     *      tags={"Career"},
     *      summary="Delete career",
     *      description="Delete career",
     *
     *       @OA\Parameter(
     *          name="id",
     *          description="career id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreCareerRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function destroy($id)
    {
        try{
            $data = $this->career->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'career id not found .'],200);
            }

            $data = $this->career->delete($id);

            return $this->response->sendSuccess($data, 'career delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }


}
