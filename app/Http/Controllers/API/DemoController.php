<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Demo\DemoRepository;
use Illuminate\Http\Request;
use Validator;

class DemoController extends Controller
{
    public function __construct(DemoRepository $demo, BaseController $response){
        $this->demo = $demo;
        $this->response = $response;
    }

     /**
     * @OA\get(
     *      path="/demo",
     *      operationId="getcareerList",
     *      tags={"Demo"},
     *      summary="Get all demo",
     *      description="Retrive the all demo by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function index()
    {
        try {
            $data = $this->demo->getAll();
            return $this->response->sendSuccess($data, 'demo list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'demo list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/demo/create",
     *      operationId="StoreDemo",
     *      tags={"Demo"},
     *      summary="Create new demo",
     *      description="Create new demo",
     *
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreDemoRequest")
     *      ),
     *
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/DemoModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *
     * )
     */
    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
                ]);

            if ($validator->fails()) {
                return $this->response->sendError('Validation Error.',['error'=>$validator->errors()],400);
            }

            $data = $this->demo->create($request->all());

            return $this->response->sendSuccess($data, 'Demo created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Demo cannot created.'],400);
        }
    }


     /**
     * @OA\get(
     *      path="/demo/{id}",
     *      operationId="getdemoDetails",
     *      tags={"Demo"},
     *      summary="Get detail of demo",
     *      description="Retrive the detail demo by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="demo id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreDemoRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->demo->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'demo retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'demo id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'demo cannot retrived .'],500);
        }
    }

    /**
     *
     * @OA\put(
     *      path="/demo/{id}",
     *      operationId="UpdateDemoDetails",
     *      tags={"Demo"},
     *      summary="Update demo",
     *      description="Update the demo by id",
     *
    *       @OA\Parameter(
    *          name="id",
    *          description="demo id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    *
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateDemoRequest")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/DemoModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function update(Request $request, $id)
    {
        try{
            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);
            }

            $data = $this->demo->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'demo id not found .'],201);
            }
            $data = $this->demo->update($request->all(), $id);
            $data = $this->demo->show($id);

            return $this->response->sendSuccess($data, 'demo updated success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

    /**
     *
     * @OA\delete(
     *      path="/demo/{id}",
     *      operationId="deletedemoDetails",
     *      tags={"Demo"},
     *      summary="Delete demo",
     *      description="Delete demo",
     *
     *       @OA\Parameter(
     *          name="id",
     *          description="demo id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreDemoRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function destroy($id)
    {
        try{
            $data = $this->demo->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'demo id not found .'],200);
            }

            $data = $this->demo->delete($id);

            return $this->response->sendSuccess($data, 'demo delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }


}
