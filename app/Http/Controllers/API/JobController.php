<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Job\JobRepository;
use Illuminate\Http\Request;
use Validator;

class JobController extends Controller
{
    public function __construct(JobRepository $job, BaseController $response){
        $this->job = $job;
        $this->response = $response;
    }

    /**
     * @OA\get(
     *      path="/job",
     *      operationId="getjobList",
     *      tags={"Job"},
     *      summary="Get all job",
     *      description="Retrive the all job by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function index()
    {
        try {
            $data = $this->job->getAll();
            return $this->response->sendSuccess($data, 'job list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'job list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/job/create",
     *      operationId="Storejob",
     *      tags={"Job"},
     *      summary="Create new job",
     *      description="Create new job",
     *
     *      @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *
     *             @OA\Schema(
     *                 allOf={
     *
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="full_name",
     *                             property="full_name",
     *                             type="string",
     *                             format="string"
     *                         )
     *                     ),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="email_address",
     *                             property="email_address",
     *                             type="string",
     *                             format="email"
     *                         )
     *                     ),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="phone_number",
     *                             property="phone_number",
     *                             type="integer",
     *                             format="integer"
     *                         )
     *                     ),
     *                      @OA\Schema(
     *                         @OA\Property(
     *                             description="address_line",
     *                             property="address_line",
     *                             type="string",
     *                             format="string"
     *                         )
     *                     ),
     *                      @OA\Schema(
     *                         @OA\Property(
     *                             description="apply_for",
     *                             property="apply_for",
     *                             type="string",
     *                             format="string"
     *                         )
     *                     ),
     *                      @OA\Schema(
     *                         @OA\Property(
     *                             description="expected_salary",
     *                             property="expected_salary",
     *                             type="integer",
     *                             format="integer"
     *                         )
     *                     ),
     *                      @OA\Schema(
     *                         @OA\Property(
     *                             description="applied_date",
     *                             property="applied_date",
     *                             type="date",
     *                             format="date"
     *                         )
     *                     ),
     *                      @OA\Schema(
     *                         @OA\Property(
     *                             description="cv_file",
     *                             property="cv_file",
     *                             type="string",
     *                             format="binary"
     *                         )
     *                     ),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="cover_letter",
     *                             property="cover_letter",
     *                             type="string",
     *                             format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/JobModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *
     * )
     */
    public function store(Request $request)
    {

        try {
            //This is for cv upload
            $file = $request->file('cv_file');
            $new_name = $request->email_address.rand() . '.' . $file->getClientOriginalExtension();
            $link1 = env('APP_URL').'/'.'public/cvupload/' . $new_name;
            $file->move(public_path('/cvupload/'), $new_name);


            //This is for cover letter
            $file = $request->file('cover_letter');
            $cover_new_name = $request->email_address.rand() . '.' . $file->getClientOriginalExtension();
            $link2 = env('APP_URL').'/'.'public/coverletter/' . $cover_new_name;
            $file->move(public_path('/coverletter/'), $cover_new_name);


            $data = $this->job->create(array_merge($request->all(),['cv_file' => $new_name, 'cover_letter' => $cover_new_name ]));

            return $this->response->sendSuccess($data, 'job created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'contact cannot created.'],400);
        }
    }

    /**
     * @OA\get(
     *      path="/job/{id}",
     *      operationId="getjobDetails",
     *      tags={"Job"},
     *      summary="Get detail of job",
     *      description="Retrive the detail job by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="job id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreJobRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->job->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'job retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'job id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'job cannot retrived .'],500);
        }
    }

}
