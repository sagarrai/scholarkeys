<?php
namespace App\Http\Controllers\API;

use App\User; 
use Validator;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth; 

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\API\BaseController;

class UserController extends Controller 
{
    public function __construct(BaseController $response){
       $this->response = $response;
    }
    
    /**
     * @OA\Post(
     *      path="/login",
     *      operationId="getLogin",
     *      tags={"User"},
     *      summary="Login method of user",
     *      description="This function has check login attempt",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/LoginUserRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    
    public function login(){ 
        
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 

            $user = Auth::user(); 
            if($user->status == 1 ) {
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                $success['roles']  = auth()->user()->permission;
                return $this->response->sendSuccess($success, 'User login successful.');
            }
            else {
                return $this->response->sendError('Unauthorised.',['error'=>'Invalid login'],401);
            }
        } 
        else{ 
            return $this->response->sendError('Unauthorised.',['error'=>'Invalid login'],401);
        } 
    }
    
    /**
     * @OA\Post(
     *      path="/register",
     *      operationId="getUserRegister",
     *      tags={"User"},
     *      summary="Register for new user",
     *      description="Returns token and user data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/RegisterUserRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    public function register(Request $request) 
    { 
        try{
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|email|unique:users', 
                'password' => 'required', 
                'c_password' => 'required|same:password', 
            ]);
            if ($validator->fails()) { 
                return $this->response->sendError('Unauthorised.',['error'=>$validator->errors()],401);            
            }
            $input = $request->all(); 
            $input['password'] = Hash::make($input['password']); 
            $user = User::create($input); 

            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;

            return $this->response->sendSuccess($success, 'User register successfully.');
        } catch (Exception $e) {

           return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],401);
        }
    }

    /**
     * @OA\Post(
     *      path="/details",
     *      operationId="getDetails",
     *      tags={"User"},
     *      summary="User details",
     *      description="This function give us user details by using token",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function details() 
    { 
        try{
            # get roles from laravel passport
            auth()->user()->getPermissionsViaRoles();
            $result = Auth::user();
            return $this->response->sendSuccess($result, 'User detail listed successfully.');
        } catch (Exception $e) {
            return $this->response->sendError('User detail not found.',['code'=>'101'],101);
        } 
    } 

    /**
     * @OA\Post(
     *      path="/roles",
     *      operationId="getRoles",
     *      tags={"User"},
     *      summary="User roles",
     *      description="User roles by using token",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function getRoles(){
        try{
            # check Using permissions via roles  https://github.com/spatie/laravel-permission/
            $roles = auth()->user()->getPermissionsViaRoles();
            return $this->response->sendSuccess($roles, 'User roles listed successfully.');
        }catch(Exception $e) {
            return $this->response->sendError('User roles not found.',[], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/allusers",
     *      operationId="allusers",
     *      tags={"User"},
     *      summary="User all list details",
     *      description="This function give us user details by using token",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function allusers(){
        try{
            $all = User::all();
            return $this->response->sendSuccess($all, 'User listed successfully.');
        }catch(Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'User list not found'], 401);

        }
    }


}