<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Career;

class CareerController extends Controller
{
   	public function index(){
    	return view('backend.career.index');
    }

    public function store(){
    	return view('backend.career.store');
    }

    public function edit($id)
    {
        $career = Career::findOrFail($id);
        return view('backend.career.edit',compact('career'));
    }
}
