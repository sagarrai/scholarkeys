<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Middleware\Cors;
use App\Model\Career;
use App\Model\Demo;
use App\User;

class DashboardController extends Controller
{
    public function index(){
    	$counts = [
            'userCount' => User::all()->count(),
            'careerCount' => Career::all()->count(),
            'demoCount' => Demo::all()->count(),
        ];

        return view('backend.index', compact('counts'));

    }
}
