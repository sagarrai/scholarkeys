<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Demo;

class DemoController extends Controller
{
    public function index(){
    	return view('backend.demo.index');
    }

    public function store(){
    	return view('backend.demo.store');
    }

    public function edit($id)
    {
        $demo = Demo::findOrFail($id);
        return view('backend.demo.edit',compact('demo'));
    }
}
