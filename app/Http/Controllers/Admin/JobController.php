<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Model\Job;

class JobController extends Controller
{
    public function index()
    {
    	return view('backend.job.index');
    }

    public function show($id)
    {
    	$job = Job::where('id',$id)->first();
        return view('backend.job.show', compact('job'));
    }

}
