<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Scholarkeys api documentation",
     *      description="L5 Swagger OpenApi for sagar",
     *      @OA\Contact(
     *          email="sagar@analogueinc.com.np"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Testing API Server"
     * )
     *
     /**
     * @OA\Get(
     *     path="/api/resource.json",
     *     @OA\Response(response="200", description="An example resource")
     * )
     *
     * @OA\Tag(
     *     name="User",
     *     description="API Endpoints of Users"
     * )
     *
     * @OA\Tag(
     *     name="Career",
     *     description="API Endpoints of Career"
     * )
     *
     * @OA\Tag(
     *     name="Demo",
     *     description="API Endpoints of Demo"
     * )
     *
     * @OA\Tag(
     *     name="Job",
     *     description="API Endpoints of Job"
     * )
     *
     * 
     */
}
