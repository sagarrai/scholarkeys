<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Career;
use App\Model\Job;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('frontend.index');
    }

    public function career()
    {
    	$careers = Career::all();
        return view('frontend.careers',compact('careers'));
    }

    public function career_details($title)
    {
        $career = Career::where('title',$title)->first();
        return view('frontend.career_details',compact('career'));
    }

    public function career_apply($title)
    {
        $career = Career::where('title',$title)->first();
        return view('frontend.career_apply',compact('career'));
    }

    public function demo()
    {
        return view('frontend.demo');
    }

    public function curriculum()
    {
        return view('frontend.curriculum');
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function quote()
    {
        return view('frontend.quote');
    }

    public function modules()
    {
        return view('frontend.modules.index');
    }

    public function survey()
    {
        return view('frontend.survey.index');
    }

    public function library()
    {
        return view('frontend.library.index');
    }

    public function webinar()
    {
        return view('frontend.webinar.index');
    }
}
