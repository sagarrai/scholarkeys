<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = [
        'title', 'no_of_opening', 'description', 'experience_qualification'
    ];
}
