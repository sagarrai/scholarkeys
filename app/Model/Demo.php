<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $fillable = [
        'full_name', 'email', 'phone_number', 'school_name', 'remarks'
    ];
}
