<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'full_name', 'phone_number' , 'email_address', 'address_line', 'apply_for',
        'expected_salary', 'apply_date', 'cv_file', 'cover_letter',
    ];
}
