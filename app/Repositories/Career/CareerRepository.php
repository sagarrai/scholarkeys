<?php 

namespace App\Repositories\Career;
use App\Model\Career;

class CareerRepository implements CareerInterface {

	protected $career;

	public function __construct(Career $career) 
	{
		$this->career = $career;
	}

	public function getAll() 
	{
		$val = $this->career->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->career->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->career->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$careerUpdate = $this->career->find($id);

		$careerUpdate->update($attributes);
		return $careerUpdate;
	}

	public function delete($id) 
	{
		$careerDelete = $this->career->destroy($id);
		return $careerDelete;
	}

}