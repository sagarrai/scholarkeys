<?php 

namespace App\Repositories\Demo;

interface DemoInterface
{
	public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function update(array $attributes, $id);

	public function delete($id);

}