<?php 

namespace App\Repositories\Demo;
use App\Model\Demo;

class DemoRepository implements DemoInterface {

	protected $demo;

	public function __construct(Demo $demo) 
	{
		$this->demo = $demo;
	}

	public function getAll() 
	{
		$val = $this->demo->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->demo->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->demo->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$demoUpdate = $this->demo->find($id);

		$demoUpdate->update($attributes);
		return $demoUpdate;
	}

	public function delete($id) 
	{
		$demoDelete = $this->demo->destroy($id);
		return $demoDelete;
	}

}