<?php

namespace App\Repositories\Job;
use \App\Mail\ToAdmin;

interface JobInterface 
{
    public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function update(array $attributes, $id);

	public function delete($id);

	public function sendmail($details, $to);

}