<?php

namespace App\Repositories\Job;
use App\Model\Job;
use \App\Mail\ToAdmin;
use App\Repositories\Job\JobInterface;
use Illuminate\Support\Facades\Mail;

class JobRepository implements JobInterface {

	protected $job;

	public function __construct(Job $job)
	{
		$this->job = $job;
	}

	public function getAll()
	{
		return $this->job->all();
	}

	public function show($id)
	{
		return $this->job->find($id);
	}

	public function create(array $attributes)
	{
		return $this->job->create($attributes);
	}

	public function update(array $attributes, $id)
	{
		$job = $this->job->find($id);
		return $job->update($attributes);
	}

	public function delete($id)
	{
		return  $this->job->destroy($id);
	}

	public function sendmail($details, $to)
	{
		Mail::to($to)->send(new ToAdmin($details));
	}

}
