<?php 

namespace App\Repositories\User;
use App\User;

class RepositoryUser implements UserInterface {

	protected $user;

	public function __construct(User $user) 
	{
		$this->user = $user;
	}

	public function getAll() 
	{
		$val = $this->user->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->user->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->user->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$userUpdate = $this->user->find($id);

		$userUpdate->update($attributes);
		return $userUpdate;
	}

	public function delete($id) 
	{
		$userDelete = $this->user->destroy($id);
		return $userDelete;
	}
}