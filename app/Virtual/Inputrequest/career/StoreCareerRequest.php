<?php 

/**
 * @OA\Schema(
 *      title="Store Career",
 *      description="Create new career ",
 *      type="object",
 *      required={"title"}
 * )
 */

class StoreCareerRequest{

     /**
     * @OA\Property(
     *     title="title",
     *     description="Title of Job",
     *     example="Project Manager"
     * )
     *
     * @var string
     */
    public $title;

      /**
     * @OA\Property(
     *     title="no_of_opening",
     *     description="Total number of opening for the job",
     *     example="1"
     * )
     *
     * @var integer
     */
    public $no_of_opening;

    /**
     * @OA\Property(
     *     title="description",
     *     description="Job description for the job",
     *     example="Must have a degree in Bachelors"
     * )
     *
     * @var string
     */
    
    public $description;

     /**
     * @OA\Property(
     *     title="experience_qualification",
     *     description="This is a for experience for the job",
     *     example="Must have 2 years of experience"
     * )
     *
     * @var string
     */
    public $experience_qualification;

}