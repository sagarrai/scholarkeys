<?php 

/**
 * @OA\Schema(
 *      title="Update demo",
 *      description="Update existing demo ",
 *      type="object",
 *      required={"full_name","email","phone_number"}
 * )
 */

class UpdateDemoRequest{

     /**
     * @OA\Property(
     *     title="full_name",
     *     description="full name for demo",
     *     example="Sagar Rai"
     * )
     *
     * @var string
     */
    public $full_name;

      /**
     * @OA\Property(
     *     title="email",
     *     description="Email for demo",
     *     example="sagar@gmail.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     title="phone_number",
     *     description="phone number for demo",
     *     example="123456789"
     * )
     *
     * @var integer
     */
    
    public $phone_number;

     /**
     * @OA\Property(
     *     title="school_name",
     *     description="School name for the demo",
     *     example="Analogue School"
     * )
     *
     * @var string
     */
    public $school_name;

    /**
     * @OA\Property(
     *     title="remarks",
     *     description="Remarks for the demo",
     *     example="lorem ipsum"
     * )
     *
     * @var string
     */
    public $remarks;

}