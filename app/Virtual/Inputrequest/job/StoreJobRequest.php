<?php 

/**
 * @OA\Schema(
 *      title="Career ",
 *      description="Create new career ",
 *      type="object",
 *      required={"full_name"}
 * )
 */

class StoreJobRequest{

    /**
     * @OA\Property(
     *     title="full_name",
     *     description="Full name of employee",
     *     example="Sagar Rai"
     * )
     *
     * @var string
     */
    public $full_name;

    /**
     * @OA\Property(
     *     title="phone_number",
     *     description="Phone number of employee",
     *     example="9816308014"
     * )
     *
     * @var integer
     */
    public $phone_number;

    /**
     * @OA\Property(
     *     title="email_address",
     *     description="Email of employee",
     *     example="sagar.raiii31@gmail.com"
     * )
     *
     * @var string
     */
    public $email_address;

    /**
     * @OA\Property(
     *     title="address_line1",
     *     description="address of employee",
     *     example="Satdobato mahalaxmi chowk"
     * )
     *
     * @var string
     */
    
    public $address_line1;

    /**
     * @OA\Property(
     *     title="apply_for",
     *     description="Apply Position",
     *     example="Manager"
     * )
     *
     * @var string
     */
    
    public $apply_for;

    /**
     * @OA\Property(
     *     title="expected_salary",
     *     description="expected salary of employee",
     *     example="300000"
     * )
     *
     * @var string
     */

    public $expected_salary;

    /**
     * @OA\Property(
     *     title="apply_date",
     *     description="Apply date fo the employee",
     *     example="2017-10-10 02:10:33"
     * )
     *
     * @var \DateTime
     */

    public $apply_date;
    
    public $cv_file;
    
    public $cover_letter;

}