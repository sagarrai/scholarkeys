<?php 

/**
 * @OA\Schema(
 *      title="Register user request",
 *      description="Register user request body data",
 *      type="object",
 *      required={"name","email","password","c_password"}
 * )
 */

class RegisterUserRequest
{
    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the new user",
     *      example="Sagar Rai"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="email",
     *      description="Email address of the new user",
     *      example="sagar@gmail.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="password",
     *      description="New password of the user (this must be alpha numeric)",
     *      example="123"
     * )
     * @var string
     */
    public $password;

        /**
     * @OA\Property(
     *      title="c_password",
     *      description="Confirm password of the user (this must be alpha numeric)",
     *      example="123"
     * )
     * @var string
     */
    public $c_password;
}