<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobAppliedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applieds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("full_name");
            $table->string("phone_number");
            $table->string("email_address");
            $table->string("address_line")->nullable();
            $table->string("apply_for")->nullable();
            $table->string("expected_salary")->nullable();
            $table->string("apply_date");
            $table->string("cv_file")->nullable();
            $table->string("cover_letter")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applieds');
    }
}
