@extends('layouts.admin-login')

@section('content')
<style>
    .btn-me {
        background-color: #3c8dbc;
    }

    #logintext {
        color: #ffffff;
    }

</style>
<div class="login-box">
    <div class="login-logo">
        {{-- <img src="/admin-resources/dist/img/logo.png" style="width:70%; height:70%"><br> --}}
        <a href="#"><b>Scholarkeys </b>Admin</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>
        
        <div style="display:none;" id="infoMesaage" role="alert" class="alert alert-success">
            Redirecting to dashbaord   
            <div class="spinner-border spinner-border-sm" role="status">
                <span class="sr-only"></span>
            </div> 
        </div>

        <form id="login-form">
            <div class="input-group mb-3">
            <input type="email" name="email" id="email" required autocomplete="email" class="form-control" placeholder="Email">
            <div class="input-group-append">
                <div class="input-group-text">
                <span class="fas fa-envelope"></span>
                </div>
            </div>
            <div id="emailMsg" class="invalid-feedback"></div>
            </div>
            <div class="input-group mb-3">
            <input type="password" id="password" class="form-control" name="password" required autocomplete="current-password" placeholder="Password">
            <div class="input-group-append">
                <div class="input-group-text">
                <span class="fas fa-lock"></span>
                </div>
            </div>
            <div id="passwordMsg" class="invalid-feedback"></div>
            </div>
            <div class="row">
            <div class="col-8">
                <div class="icheck-primary">
                <input type="checkbox" id="remember">
                <label for="remember">
                    Remember Me
                </label>
                </div>
            </div>
            <div class="col-4">
                <button type="button" id="login-btn" class="btn btn-me btn-block"> 
                <span id="logintext"> Sign in </span>  
                <div class="spinner-border spinner-border-sm" id="spinner_login" style="display:none; " role="status">
                        <span class="sr-only">Loading...</span>
                    </div> </button>
            </div>
            </div>
        </form>

        <!-- <div id="getRequestData"></div> 

        <div class="social-auth-links text-center mb-3">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-primary">
            <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
            </a>
            <a href="#" class="btn btn-block btn-danger">
            <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
            </a>
        </div> -->
        <!-- /.social-auth-links -->

        <!-- <p class="mb-1">
            <a href="#">I forgot my password</a>
        </p> -->
        {{-- <p class="mb-0">
            <a href="{{ route('register') }}" class="text-center">Register a new admin membership</a>
        </p> --}}
        </div>
        <!-- /.login-card-body -->
    </div>
    </div>
<!-- /.login-box -->
@endsection
