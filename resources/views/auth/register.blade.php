@extends('layouts.admin-register')
@section('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<div class="register-box">
    <div class="register-logo">
        {{-- <img src="/admin-resources/dist/img/logo.png" style="width:70%; height:70%"><br> --}}
        <a href="#"><b>Scholarkeys</b> Admin</a>
    </div>
    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">{{ __('Register') }} a new membership</p>
            <div style="display:none;" id="registered-msg" role="alert" class="alert alert-success">
                <strong>Success</strong>
                <p>Your Account has been register successfully ! to continue please <a href="{{Url('/admin/login')}}">login</a>   </p>
                <hr>
                <p class="text-center">Redirecting  your in few momoents.... <div class="spinner-border spinner-border-sm" id="spinner_signup" style="display:none; " role="status">
                    <span class="sr-only">Loading...</span>
                </div>  </p>
                
            </div>
            <div style="display:none;" id="not-register-msg" role="alert" class="alert alert-danger">
                <strong>There went something wrong in form ! </strong>
            </div>
            
            <form id="register-form" method="POST">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" id="name" name="name" value="{{ old('name') }}" required autocomplete="name" class="form-control" placeholder="Full name">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    <div id="nameMsg" class="invalid-feedback"></div>
                </div>
                <div class="input-group mb-3">
                    <input type="email" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    <div id="emailMsg" class="invalid-feedback"></div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" id="password" name="password" required class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    <div id="passwordMsg" class="invalid-feedback"></div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" id="c_password" class="form-control" name="c_password" required placeholder="Retype password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    <div id="c_passwordMsg" class="invalid-feedback"></div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" checked id="agreeTerms" name="terms" value="agree">
                            <label for="agreeTerms">
                                I agree to the <a href="#">terms</a>
                            </label>
                            <div id="agreeTermsMsg" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="button" id="register-btn" class="btn btn-primary btn-block">
                        <span id="registerText">Register</span>
                        <div class="spinner-border spinner-border-sm" id="spinnerSignupBth" style="display:none; " role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            </form>
            <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
        </div>
        <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->
    @endsection