@extends('layouts.admin')
@section('title','Add Career')
@section('content')
<link rel="stylesheet" href="/admin-resources/plugins/summernote/summernote-bs4.css">

<div class="content">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Career</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Career Add</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <form role="form" id="career_form" method="POST" enctype="multipart/form-data" action="<?php echo URL::to('/') ?>/api/career">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" name="title" class="form-control" placeholder="Title for the career">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Number of opening</label>
                                            <input type="number" name="no_of_opening" class="form-control" placeholder="Number of opening">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Career Description</label>
                                            <textarea class="textarea" name="description" placeholder="Place some text here"
                                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" placeholder="Description for the Career"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Experience and Qualification</label>
                                            <textarea class="textarea" name="experience_qualification" placeholder="Place some text here"
                                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" placeholder="Experience and Qualification for the Career"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input">
                                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success" type="submit">
                                    <span class="spinner-border spinner-border-sm" id="spinner_add" role="status" aria-hidden="true"></span>
                                Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="/admin-resources/plugins/summernote/summernote-bs4.min.js"></script>
<script src="/admin-resources/dist/js/global.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    $("#spinner_add").hide();
    $('.textarea').summernote();
    
    $('#career_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");
        var base_url = get_baseurl();

        $.ajax({
        headers: {  
            'Authorization': `Bearer ${token}`,
        },

        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:true,

        beforeSend: function(){
            $("#spinner_add").show();
        },
        complete: function(){
            $("#spinner_add").hide();
        },
        success: function(response)
        {
            if(response.code == 200 && response.status== true) 
            {
                toastr.success('Added Successfully');
                window.setTimeout(function () {
                    window.location.href = base_url + '/admin/career/allcareers';
                }, 1500);
            }
            else 
            {
                toastr.error('Something went wrong while submitting the career');
                window.setTimeout(function () {
                    window.location.href = base_url + '/admin/career/allcareers';
                }, 1500);
            }
        }
        });
        });

    });
</script>
@endsection