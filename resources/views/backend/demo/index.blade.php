@extends('layouts.admin') @section('title', 'Demo') @section('content')
<!-- Main content -->
<section class="content ">
    <!-- Default box -->
    <br>
    <div class="card mt-2">
        <div class="card-header">
            <h3 class="card-title">Demo Lists</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body p-2">
            <table class="table table-striped projects" id="demoListTableId">
                <thead>
                    <tr>
                        <th width="10%">
                            S.No.
                        </th>
                        <th width="10%">
                            Full Name
                        </th>
                        <th width="15%">
                            Email
                        </th>
                        <th width="10%">
                            Phone Number
                        </th>
                        <th width="20%">
                            School Name
                        </th>
                        <th width="25%">
                            Remarks
                        </th>
                        <th width="10%">
                            Action
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="render-here">Are you sure to delete this contact?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="demo_delete" value=""  class="btn btn-danger demo_delete">Delete</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<script src="/admin-resources/plugins/jquery/jquery.min.js"></script>
<script src="/admin-resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin-resources/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="/admin-resources/plugins/toastr/toastr.min.js"></script>
<script src="/admin-resources/dist/js/adminlte.min.js"></script>
<script type="text/javascript">
    $(function () {
    LoadTable();
    $(document).keypress(function (e) {
    if (e.which == 5) 
    {
        LoadTable();
    }
        });
    })

    function LoadTable() {
    var i= 1;
    var base_url = get_baseurl();
    $('#demoListTableId').dataTable().fnDestroy();
    
    $('#demoListTableId').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
        "ajax": 
        {
            "type": "GET",
            "url": base_url + "/api/demo",
            "contentType": "application/json; charset=utf-8",
            "headers": 
            {
                'Authorization': `Bearer ${getCookie("token")}`,
            },
            "data": function (data) {
            data.SearchContent = $('#SearchContent').val();
            return JSON.stringify(data);
            }
        },
        "columns": [
            {
              "render": function(data, type, full, meta) {
                return i++;
              }
            },
            {"data": "full_name" },
            {"data": "email" },
            {"data": "phone_number" },
            {"data": "school_name" },
            {"data": "remarks" },
            {
                "data": "id", "title": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
        })
    }

        var enable = function (data, type, full, meta) {
        $.fn.dataTable.render.text();
        var actionBtn = "";
        actionBtn = '&nbsp;&nbsp; <button type="button" onClick="bdelete('+full.id+')" data-target="#modal-default" data-toggle="modal" class="btn btn-danger btn-sm btn-update">Delete</a>';
        return actionBtn;
        }

        function bdelete(id){
            $("#demo_delete").val(id);
        }
        
        $(".demo_delete").click(function() {
        var base_url = get_baseurl();
        
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        var id = $(this).val();
        var token = getCookie("token");
        var url = base_url + '/api/demo/'+ id;
        $.ajax({
            type: 'DELETE',
            url: url,
            headers: {
              'Authorization': `Bearer ${getCookie("token")}`,
            },
            success: function (response) 
            {
                if (response.code == "200") 
                {
                    window.setTimeout(function () 
                    {
                        window.location.href = base_url +'/admin/demo/alldemos';
                    }, 1000 );
                    toastr.success('Deleted Successfully !!');
                }
            }
        });
        });
        
</script>
@endsection