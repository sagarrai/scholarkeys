@extends('layouts.admin') @section('title', 'Job Applied') @section('content')

@section('content')

    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h2>Job Detail &nbsp; &nbsp;  <div class="spinner-border" id="spinner_add" role="status" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div></h2>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a id="backLink" href="{{url('admin/job/')}}">Job</a></li>
                        <li class="breadcrumb-item active">Job Detail</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

    <section class="content">
    <div class="card">
        <div class="card-header">
            <h3 id="fullName"></h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                    <div class="row">
                        <div class="col-12 col-sm-10">
                            <div class="info-box bg-light">
                                <div class="info-box-content">
                                    <span class="info-box-text text-center text-muted">Email ID</span>
                                    <span class="info-box-number text-center text-muted mb-0" id="emailid"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-10">
                            <div class="info-box bg-light">
                                <div class="info-box-content">
                                    <span class="info-box-text text-center text-muted">Phone Number</span>
                                    <span class="info-box-number text-center text-muted mb-0" id="PhoneNumber"><span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                {{-- <h4 id="fullName"></h4> --}}
                                <div class="post">
                                    <div class="user-block">
                                        {{-- <img class="img-circle img-bordered-sm" src="/frontend/dist/img/user1-128x128.jpg" alt="user image"> --}}
                                        {{-- <span class="username">
                                            <a href="#" id="fullName"></a>
                                        </span> --}}
                                        Applied Date: <span id="applyDate"></span>
                                    </div>
                                    {{-- <p>
                                        Lorem ipsum represents a long-held tradition for designers,
                                        typographers and the like. Some people hate it and argue for
                                        its demise, but others ignore.
                                    </p> --}}
                                    {{-- <p>
                                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v2</a>
                                    </p> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                        <h3 class="text-primary"><i class="fas fa-paint-brush"></i> Applicant Details</h3>
                        {{-- <p class="text-muted" id="applyFor"></p> --}}
                        <div class="text-muted">
                            <p class="text-sm">Expected Salary
                                <b class="d-block" id="salary"></b>
                            </p>
                        </div>
                        <h5 class="mt-5 text-muted">Applied For</h5>
                        <ul class="list-unstyled">
                            <li>
                                {{-- <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Functional-requirements.docx</a> --}}
                                <p class="text-muted" id="applyFor"></p>
                            </li>
                        </ul>
                        <div class="mt-5 mb-3">
                            <a href="#" id="file" class="btn btn-md btn-warning">See CV </a>
                            <a href="#" id="cover_letter" class="btn btn-md btn-info">Cover Letter</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<input type="hidden" id="posturl" value="{{Url('api/job')}}/{{Request::route('id')}}"/>
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
            
        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get", 
            url: $("#posturl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function() { 
                $("#spinner_add").show();
            },

            complete: function(){ 
                $("#spinner_add").hide();
            },

            success: function(response){ 
                if(response.code == 200 && response.status==true) {
                    var result = response.data;
                    $("#topName").html(result.full_name);
                    $("#fullName").html(result.full_name);
                    $("#PhoneNumber").html(result.phone_number);
                    $("#emailid").html(result.email_address);
                    $("#applyFor").html(result.apply_for);
                    $("#salary").html(result.expected_salary);
                    $("#applyDate").html(result.apply_date);
                    $("#file").attr('href',"{{ URL::to('/') }}/cvupload/"+ result.cv_file);
                    $("#cover_letter").attr('href',"{{ URL::to('/') }}/coverletter/"+ result.cover_letter);
                }
                else {
                    window.location.href = $("#backLink").attr("href");
                }
            }
        });

    });

</script>

@endsection
