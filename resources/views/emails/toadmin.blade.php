<!DOCTYPE html>
<html>
<head>
    <title>ScholarKeys</title>
</head>
<body>
    <p> <strong>Full Name: </strong> {{ $details['full_name'] }} </p>
    <p> <strong>Phone: </strong> {{ $details['phone_number'] }} </p>
    <p> <strong>Email: </strong>{{ $details['email_address'] }}</p>
    <p> <strong>Address Line: </strong> {{ $details['address_line'] }} </p>
    <p> <strong>Applied For: </strong> {{ $details['apply_for'] }} </p>
    <p> <strong>Expected Salary: </strong> {{ $details['expected_salary'] }} </p>
    <p> <strong>CV: </strong> {{ $details['cv_file'] }} </p>
    <p> <strong>Cover Letter: </strong> {{ $details['cover_letter'] }} </p>
</body>
</html>