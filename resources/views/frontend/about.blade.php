@extends('layouts.frontend')

@section('content')

	<section class="section is-page-intro">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="title-heading mt-4">
						<h1 class="heading mb-3">About <span class="text-primary">Scholar Keys</span></h1>
						<p class="text-muted para-desc mx-auto mb-0">ScholarKeys comes with <strong class="text-primary">all in one ERP solution</strong> for your Educational Institutes that will reshape the teaching and learning experience.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
					<div class="position-relative">
						<img src="/frontend/images/about_vision.png" class="rounded img-fluid mx-auto d-block" alt="">
					</div>
				</div>

				<div class="col-lg-7 col-md-7 mt-4 pt-2 mt-md-0 pt-sm-0">
					<div class="section-title ml-lg-4">
						<h4 class="title mb-3 mb-md-4">The <span class="text-primary">Vision</span></h4>
						<p class="text-muted para-desc mx-auto mb-0 text-justify">Amid Covid-19 pandemic, a lot of business took a drastic hit on the market. With that came, lack in taking the academic year of children further. We asked ourselves!!! If people can work from home and get productivity out of their work, why can’t we do that with the education system? Behind every successful work from home campaign is a platform where all the team can collaborate. Likewise with ScholarKeys, we are creating a platform for the education system to pull off “study from home” effectively and make the most out of technology.  We can all agree to the fact that with technology taking over, virtual learning is key to changing how we teach and learn. Now ScholarKeys brings you an e-learning platform where we bring education and technology hand in hand.</p>
					</div>
				</div>
			</div>

			<div class="row align-items-center mt-5">
				<div class="col-lg-7 col-md-7 mt-4 pt-2 mt-md-0 pt-sm-0 order-2 order-md-1">
					<div class="section-title ml-lg-4">
						<h4 class="title mb-3 mb-md-4">Key <span class="text-primary">Mission</span></h4>
						<p class="text-muted para-desc mx-auto text-justify">We want our nation to be prepared with whatever the world has to offer. Even post pandemic, we can never be sure that this won’t happen again? Our ultimate vision with ScholarKeys is to provide our nation with all in one platform where learning and teaching can both be productive and effective. In the nation full of school management software, we want to provide the education system a platform for higher learning and innovative collaboration.</p>
						<p><strong>If you think education is expensive, try ignorance.</strong></p>
					</div>
				</div>

				<div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0 order-1 order-md-2">
					<div class="position-relative">
						<img src="/frontend/images/about_mission.jpg" class="rounded img-fluid mx-auto d-block" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section border-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="section-title">
						<h4 class="title mb-4">Amazing features of <span class="text-primary">ScholarKeys</span></h4>
					</div>
					<ul class="list-unstyled feature-list text-muted mb-0">
						<li>
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle fea icon-sm text-success mr-2">
								<path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
								<polyline points="22 4 12 14.01 9 11.01"></polyline>
							</svg>
							Interactive courses, lectures, and assignments
						</li>
						<li>
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle fea icon-sm text-success mr-2">
								<path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
								<polyline points="22 4 12 14.01 9 11.01"></polyline>
							</svg>
							Interactive Dashboard that notifies students, teachers, administrators and guardians
						</li>
						<li>
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle fea icon-sm text-success mr-2">
								<path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
								<polyline points="22 4 12 14.01 9 11.01"></polyline>
							</svg>
							Forums to discuss difficulties students encounter in assignments
						</li>
						<li>
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle fea icon-sm text-success mr-2">
								<path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
								<polyline points="22 4 12 14.01 9 11.01"></polyline>
							</svg>
							Monitor students’ progress easily
						</li>
						<li>
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle fea icon-sm text-success mr-2">
								<path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
								<polyline points="22 4 12 14.01 9 11.01"></polyline>
							</svg>
							Conducting examinations, grading students, and handing our reports was never smoother
						</li>
						<li>
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle fea icon-sm text-success mr-2">
								<path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
								<polyline points="22 4 12 14.01 9 11.01"></polyline>
							</svg>
							Provides teachers with a greater license to design new curriculum
						</li>
					</ul>
					<div class="mt-4">
						<a href="demo.php" class="btn btn-primary mt-2 mr-2">Try Our Free Demo <i class="mdi mdi-arrow-right"></i></a>
						<a href="modules.php" class="btn btn-outline-primary mt-2 mr-2">View Our Modules <i class="mdi mdi-arrow-right"></i></a>
					</div>
				</div>
				<div class="col-md-6">
					<img src="/frontend/images/about_features.jpg" class="img-fluid rounded" alt="">
				</div>
			</div>
		</div>
	</section>

@endsection
	