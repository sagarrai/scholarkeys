@extends('layouts.frontend')

@section('content')

<section class="section is-page-intro">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="title-heading mt-4">
						<h1 class="heading mb-0">Apply as <span class="text-primary">{{ $career->title }}</span></h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9 col-12">
					<p class="text-muted mb-5">We're glad to learn that you're interested in working with us. ScholarKeys team is always looking to grow its family and we'd be more than happy to have you among us. To apply for the position you're interested in, please complete the form below.</p>

					<form class="needs-validation" novalidate id="apply_form" method="POST">

						<div class="form-row">
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="name">Full Name</label>
									<input type="text" class="form-control" name="full_name" required="required">
									<div class="invalid-feedback">Please enter your name.</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="email">Email Address</label>
									<input type="email" class="form-control" name="email_address" required="required">
									<div class="invalid-feedback">Please enter your valid email address.</div>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="phone">Phone Number</label>
									<input type="text" class="form-control" name="phone_number" required="required">
									<div class="invalid-feedback">Please enter your valid phone number.</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="location">Where do you live? (optional)</label>
									<input type="text" class="form-control" name="address_line">
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="salary">Expected Salary</label>
									<input type="number" class="form-control" name="expected_salary" required="required">
									<div class="invalid-feedback">Please enter your expected salary.</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="name">What position are you applying for?</label>
									<input type="text" class="form-control" name="apply_for" required="required" value="{{ $career->title }}">
								</div>
							</div>
						</div>
						<input type="hidden" value="<?php echo date("Y-m-d H:m");?>" name="apply_date">
						<div class="form-row">
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="salary">Upload your CV</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="cv_file" id="cv_file" required="required">
											<label class="custom-file-label" for="uploadcv">Choose file</label>
										</div>
										<div class="input-group-append">
											<button class="btn btn-outline-primary" type="button">Upload</button>
										</div>
									</div>
									<div class="invalid-feedback">Please upload your valid CV.</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="salary">Upload your cover letter</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="cover_letter" id="cover_letter" required="required">
											<label class="custom-file-label" for="uploadcv">Choose file</label>
										</div>
										<div class="input-group-append">
											<button class="btn btn-outline-primary" type="button">Upload</button>
										</div>
									</div>
									<div class="invalid-feedback">Please upload you valid cover letter.</div>
								</div>
							</div>
						</div>

						<p class="text-center mt-4 mb-0">
							<button type="submit" class="btn btn-primary">Apply Now<span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span></button>
							<button type="reset" class="btn btn-outline-primary ml-3">Start Over</button>
						</p>
					</form>

				</div>
			</div>
		</div>
	</section>

	<script src="/admin-resources/dist/js/global.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#spinner").hide();
    function get_baseurl() {
    var base_url = "<?php echo URL::to('/') ?>";
        return base_url;
    }

    $('#apply_form').on('submit', function(event){
        event.preventDefault();
        toastr.options.timeOut = 10000;

        var base_url = get_baseurl();

            $.ajax({
            url: base_url + '/api/job/create',
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,

            beforeSend: function(){
                $("#submit-btn").attr("disabled","disabled");
                $("#spinner").show();
            },

            complete: function(){
                $("#submit-btn").removeAttr("disabled","disabled");
                $("#spinner").hide();
            },

                success:function(data)
                {
                    if(data.code == 200 && data.status == true){
    					$("#spinner").hide();
                        // var parent = document.getElementById('box');
                        // var span = document.createElement('span');
                        // span.setAttribute('class', 'alert alert-success');
                        // span.innerHTML = 'Sent Successfully';
                        // parent.parentNode.insertBefore(span,parent);
                        toastr.success('Sent Successfully');
                        window.setTimeout(function () {
                            window.location.href = base_url +'/careers';
                        }, 2000 );

                    }else{
                        toastr.error('Error');
                    }


                }
            })
        });
    });
</script>

@endsection