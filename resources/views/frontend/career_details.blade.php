@extends('layouts.frontend')

@section('content')

	<section class="section is-page-intro">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="title-heading mt-4">
						<h1 class="heading mb-3">{{ $career->title }}</h1>
						<p class="text-muted mb-0 lead">Number of Openings &mdash; 1</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9 col-12">
					<h4 class="title mb-3">Job <span class="text-primary">Summary</span></h4>
					
					{!! $career->description !!}

					<h4 class="title mb-3">Experience & <span class="text-primary">Qualifications</span></h4>
					
					{!! $career->experience_qualification !!}

					<p><a href="/career_apply/{{ $career->title }}" class="btn btn-primary btn-lg">Apply Now</a></p>

				</div>
			</div>
		</div>
	</section>


@endsection
	