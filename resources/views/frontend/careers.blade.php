@extends('layouts.frontend')

@section('content')

	<section class="section is-page-intro">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="title-heading mt-4">
						<h1 class="heading mb-0">Our <span class="text-primary">Careers</span></h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<h3 class="title text-center mb-3 mb-md-5">Check out our current <span class="text-primary">openings</span></h3>
			<div class="row justify-content-center">
				<div class="col-lg-9 col-12 career-counter">
					<div class="border-bottom"></div>
					@foreach($careers as $career)
						<div class="career-counter-block d-block d-lg-flex align-items-center justify-content-between border-bottom py-4">
							<div>
								<p class="lead mb-3 lh-1"> {{ $career->title }} </p>
								<p class="text-muted m-0">Number of openings <b class="mx-1">&mdash;</b> <strong class="text-dark">{{ $career->no_of_opening }} </strong></p>
								<p class="text-muted m-0">Expires on <strong class="text-dark">15th July 2020</strong></p>
							</div>
							<a href="/career_details/{{ $career->title }}" class="btn btn-outline-primary mt-lg-0 mt-4">Details</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</section>

@endsection