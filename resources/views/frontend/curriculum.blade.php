@extends('layouts.frontend')

@section('content')

<style>
		@font-face {
			font-display: block;
			font-family: Roboto;
			src: url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/7529907e9eaf8ebb5220c5f9850e3811.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/25c678feafdc175a70922a116c9be3e7.woff) format("woff")
		}
		@font-face {
			font-display: fallback;
			font-family: Roboto;
			font-weight: 600;
			src: url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/6e9caeeafb1f3491be3e32744bc30440.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/71501f0d8d5aa95960f6475d5487d4c2.woff) format("woff")
		}
		@font-face {
			font-display: fallback;
			font-family: Roboto;
			font-weight: 700;
			src: url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/3ef7cf158f310cf752d5ad08cd0e7e60.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/ece3a1d82f18b60bcce0211725c476aa.woff) format("woff")
		}
		#sib-container input:-ms-input-placeholder {
			text-align: left;
			font-family: "Helvetica", sans-serif;
			color: #c0ccda;
			border-width: px;
		}
		#sib-container input::placeholder {
			text-align: left;
			font-family: "Helvetica", sans-serif;
			color: #c0ccda;
			border-width: px;
		}
		#sib-container textarea::placeholder {
			text-align: left;
			font-family: "Helvetica", sans-serif;
			color: #c0ccda;
			border-width: px;
		}
	</style>
	<link rel="stylesheet" href="https://sibforms.com/forms/end-form/build/sib-styles.css">

	<section class="section is-page-intro">
		<div class="container">
			<div class="title-heading text-center mt-4">
				<h1 class="heading mb-3">Curriculum</h1>
				<p class="text-muted para-desc mx-auto mb-0">Instead of a National curriculum for education, what we really need is an <strong class="text-primary">individual curriculum</strong> for every child.</p>
			</div>

			<div class="row align-items-center mt-5 pt-3">
				<div class="col-lg-6 col-md-7 mt-4 pt-2 mt-md-0 pt-sm-0 order-2 order-md-1">
					<div class="section-title ml-lg-4">
						<h4 class="title mb-3 mb-md-4">Key <span class="text-primary">Mission</span></h4>
						<p class="text-muted para-desc mx-auto text-justify">What ScholarKeys believes is, we should never judge a fish by its ability to climb a tree. Having said that, we are coming up with a curriculum module that not only allows Educational institutes to prepare curriculum based on their education system, but also present our education system with a curriculum that will reshape the learning experience. Curriculum module allows you to connect with students and their parents to seamlessly collaborate and check the assignments in one platform. Now talk about one stop solution, think ScholarKeys.</p>
					</div>
				</div>

				<div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0 order-1 order-md-2">
					<div class="position-relative">
						<img src="frontend/images/curriculum_1.jpg" class="rounded img-fluid mx-auto d-block" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 col-md-10 mt-4 pt-2 mt-sm-0 pt-sm-0">
					<h4 class="title mb-3 mb-md-4">The <span class="text-primary">future</span> of ScholarKeys & its Curriculum</h4>
					<p class="text-muted">ScholarKeys is finalizing the content and information for the ScholarKeys Curriculum, so please feel free to check back again soon.</p>
					<p class="text-muted">That being said, we are planning to list out other forms of curriculum as well in a later date. The curricula that we will be adding in the near future are as follows -</p>
					<ul class="list-unstyled feature-list text-muted">
						<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Study Curriculum</li>
						<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Assessment Curriculum</li>
						<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Evaluation Curriculum</li>
					</ul>

					<div class="rounded bg-primary mt-100 p-lg-5 p-4">
						<div class="section-title text-center">
							<h2 class="text-white mb-4">Stay Updated</h2>
							<p class="text-white w-75 mx-auto mb-5">If you want to stay updated with our plans with the Curriculum and how it will evolve in the future, feel free to drop your email down below -</p>

							<div class="sib-form p-0" style="text-align: center; background-color: transparent;">
								<div id="sib-form-container" class="sib-form-container">
									<div id="error-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:'Helvetica', sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;max-width:540px; border-width:px;">
										<div class="sib-form-message-panel__text sib-form-message-panel__text--center">
											<svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
												<path d="M256 40c118.621 0 216 96.075 216 216 0 119.291-96.61 216-216 216-119.244 0-216-96.562-216-216 0-119.203 96.602-216 216-216m0-32C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm-11.49 120h22.979c6.823 0 12.274 5.682 11.99 12.5l-7 168c-.268 6.428-5.556 11.5-11.99 11.5h-8.979c-6.433 0-11.722-5.073-11.99-11.5l-7-168c-.283-6.818 5.167-12.5 11.99-12.5zM256 340c-15.464 0-28 12.536-28 28s12.536 28 28 28 28-12.536 28-28-12.536-28-28-28z"
												/>
											</svg>
											<span class="sib-form-message-panel__inner-text">Your information could not be sent. Please try again.</span>
										</div>
									</div>
									<div></div>
									<div id="success-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:'Helvetica', sans-serif; color:#085229; background-color:#e7faf0; border-radius:3px; border-width:px; border-color:#13ce66;max-width:540px; border-width:px;">
										<div class="sib-form-message-panel__text sib-form-message-panel__text--center">
											<svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
												<path d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
												/>
											</svg>
											<span class="sib-form-message-panel__inner-text">Thank you for wishing to stay updated. We will get let you know once the curriculum is updated.</span>
										</div>
									</div>
									<div></div>
									<div id="sib-container" class="sib-container--large sib-container--vertical p-0" style="text-align:center; background-color:transparent; max-width:540px; border-width:0px; border-color:#C0CCD9; border-style:solid;">
										<form id="sib-form" method="POST" action="https://486ae310.sibforms.com/serve/MUIEAAWIY7iTkrA7QTgOvgT4FRzGUZ51Vs0rH144S0OztpKBGirCrOL7VQIUEToehgQQlsCwApKucN2ViFibuyDECndExsyrp4G1c_cMUtyl-jen7O1ENwMWRXiDuKlP123aBYFEA-JkaO9D_N-Z_Tn05XN_Wa3FXJA8_Vvx_6E9kSSNTZyL_X2DGyx9TYKQqjYgY1pdXFTq3Wsj"
											data-type="subscription">
											<div class="mb-3">
												<div class="sib-input sib-form-block">
													<div class="form__entry entry_block">
														<div class="form__label-row ">
															<label class="entry__label mb-0" style="font-size:18px; text-align:left; font-weight:700; font-family:'Helvetica', sans-serif; color:#002244; border-width:px;" for="EMAIL" data-required="*">
																Email Address
															</label>

															<div class="entry__field">
																<input class="input border-0" type="text" id="EMAIL" name="EMAIL" autocomplete="off" data-required="true" required />
															</div>
														</div>

														<label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:'Helvetica', sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
														</label>
													</div>
												</div>
											</div>
											<div class="mb-3">
												<div class="sib-captcha sib-form-block">
													<div class="form__entry entry_block">
														<div class="form__label-row ">
															<script>
																function handleCaptchaResponse() {
																	var event = new Event('captchaChange');
																	document.getElementById('sib-captcha').dispatchEvent(event);
																}
															</script>
															<div class="g-recaptcha sib-visible-recaptcha" id="sib-captcha" data-sitekey="6LdFi_oUAAAAABFGt5JAyxR8MlG2ZfXwWG716ZoU" data-callback="handleCaptchaResponse"></div>
														</div>
														<label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:'Helvetica', sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
														</label>
													</div>
												</div>
											</div>
											<div class="mb-3">
												<div class="sib-form-block" style="text-align: left">
													<button class="sib-form-block__button sib-form-block__button-with-loader" style="font-size:16px; text-align:left; font-weight:700; font-family:'Helvetica', sans-serif; color:#FFFFFF; background-color:#002244; border-radius:3px; border-width:0px;"
														form="sib-form" type="submit">
														<svg class="icon clickable__icon progress-indicator__icon sib-hide-loader-icon" viewBox="0 0 512 512">
															<path d="M460.116 373.846l-20.823-12.022c-5.541-3.199-7.54-10.159-4.663-15.874 30.137-59.886 28.343-131.652-5.386-189.946-33.641-58.394-94.896-95.833-161.827-99.676C261.028 55.961 256 50.751 256 44.352V20.309c0-6.904 5.808-12.337 12.703-11.982 83.556 4.306 160.163 50.864 202.11 123.677 42.063 72.696 44.079 162.316 6.031 236.832-3.14 6.148-10.75 8.461-16.728 5.01z"
															/>
														</svg>
														Keep me updated
													</button>
												</div>
											</div>

											<input type="text" name="email_address_check" value="" class="input--hidden">
											<input type="hidden" name="locale" value="en">
										</form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

@endsection