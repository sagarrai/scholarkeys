@extends('layouts.frontend')

@section('content')

	<section class="section is-page-intro">
		<div class="container">
			<div class="section-title mt-3 mb-5 pb-2 text-center">
				<h1 class="heading mb-0">Try our <span class="text-primary">Demo</span></h1>
				<p class="text-muted mb-0">Tell us a little about yourself and we'll email you a <span class="text-primary font-weight-bold">demo link</span>.</p>
			</div>

			<div class="row justify-content-center">
				<div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0 order-2 order-md-1">

					<div class="alert alert-success mb-4" id="demo_link" role="alert">
						<p class="mb-0 lh-1-4">Thank you for showing interest in ScholarKeys. You can now proceed to the <a href="#" target="_blank" class="alert-link">demo link here</a>.</p>
					</div>

					{{-- <div class="alert alert-danger mb-4" role="alert">
						<p class="mb-0 lh-1-4">There was an error while processing your request. Did you put in a valid email address? Alternatively, you can call us at <a href="tel:+9779802320803" class="font-weight-bold text-white border-bottom">+977 98023 20803</a>.</p>
					</div> --}}

					<form class="needs-validation" novalidate id="demo_form">
						<div class="form-group">
							<label for="name">Full Name</label>
							<input type="text" class="form-control" id="full_name" required="required">
							<div class="invalid-feedback">Please enter your name.</div>
						</div>

						<div class="form-row">
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="email">Email Address</label>
									<input type="email" class="form-control" id="email" required="required">
									<div class="invalid-feedback">Please enter your valid email address.</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group">
									<label for="phone_number">Phone Number</label>
									<input type="text" class="form-control" id="phone_number" required="required">
									<div class="invalid-feedback">Please enter your valid phone number.</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="school">School Name</label>
							<input type="text" class="form-control" id="school_name" required="required">
							<div class="invalid-feedback">Please enter your school name.</div>
						</div>
						<div class="form-group">
							<label for="remarks">Remarks <span class="text-muted">(optional)</span></label>
							<textarea class="form-control" id="remarks" rows="5"></textarea>
						</div>
						<div class="form-group">
							<div class="g-recaptcha" data-sitekey="6LdFi_oUAAAAABFGt5JAyxR8MlG2ZfXwWG716ZoU"></div>
						</div>

						<button type="submit" class="btn btn-primary">Get My Demo<span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span></button>
						<button type="reset" class="btn btn-outline-primary ml-3">Start Over</button>
					</form>

				</div>
				<div class="col-md-6 order-1 order-md-2">
					<h4 class="title mb-3 mb-md-4">We value your <span class="text-primary">time</span></h4>
					<p class="text-muted para-desc mx-auto mb-5">We thank you for making time in your busy schedule to look into <strong class="text-primary">ScholarKeys</strong>. We want to give you further details and information, as well as guide on how <strong class="text-primary">ScholarKeys</strong> works and what more you can achieve. To do that, we will need a few essential information from you before we can proceed.</p>
					<div class="my-5 d-none d-md-block"><img src="/frontend/images/demo.png" class="img-fluid rounded" alt=""></div>
				</div>
			</div>
		</div>
	</section>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script>
    $(document).ready(function () {
        function get_baseurl() {
            var base_url = "<?php echo URL::to('/') ?>";
            return base_url;
        }

        $('#demo_link').hide();
        $('#spinner').hide();

        $('#demo_form').on('submit',function(e){
        e.preventDefault();

            var full_name = $("#full_name").val();
            var phone_number = $("#phone_number").val();
            var email = $("#email").val();
            var school_name = $("#school_name").val();
            var remarks = $("#remarks").val();
            var base_url = get_baseurl();
            toastr.options.timeOut = 4000;

            $.ajax({
                url: base_url + '/api/demo/create',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    "_token" : "{{ csrf_token() }}",
                    "full_name": full_name,
                    "phone_number": phone_number,
                    "email": email,
                    "school_name": school_name,
                    "remarks": remarks,
                },

                beforeSend: function(){ 
                	$("#spinner").show();
	            },

	            complete: function(){ 
	                $("#spinner").hide();
	            },
                success:function(data){
                    toastr.success("Sent Successfully");
                	$('#demo_form').hide();
                	$('#demo_link').show();
                } 

            });
        });
    });

</script>

@endsection
	