@extends('layouts.frontend')

@section('content')

<section class="bg-half-170 w-100 overflow-hidden" id="home" style="background: url('/frontend/images/home-shape.png') center center;">
		<div class="container">
			<div class="row align-items-center pt-5">
				<div class="col-xl-6 col-lg-6 col-md-7">
					<div class="title-heading">
						<h1 class="heading mb-3 lh-1">The key to a learning platform</h1>
						<p class="para-desc text-muted">We now present to you the software you have been waiting for. ScholarKeys comes with all in one ERP solution for your Educational Institutes that will reshape the teaching and learning experience.</p>
						<p class="mt-4 mb-5">
							<a href="demo.php" class="btn btn-primary">Try Our Free Demo <i class="mdi mdi-chevron-right"></i></a>
						</p>
					</div>
				</div>

				<div class="col-xl-6 col-lg-6 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
					<div class="classic-saas-image position-relative">
						<div class="bg-saas-shape no-shape position-relative">
							<img src="/frontend/images/home.png" alt="Scholar keys home" class="mx-auto d-block splash-img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="row align-items-center justify-content-sm-center">
						<div class="col-lg-6 col-md-4 col-sm-5 col-6 mt-0 mt-lg-5 pt-0 pt-lg-3">
							<div class="card work-container work-modern overflow-hidden rounded border-0 shadow-lg">
								<div class="card-body p-0">
									<img src="/frontend/images/home_about_01.png" class="img-fluid" alt="Scholar keys work">
									<div class="overlay-work bg-primary"></div>
								</div>
							</div>
						</div>

						<div class="col-lg-6 col-md-4 col-sm-5 col-6 mb-0 mb-lg-5">
							<div class="card work-container work-modern overflow-hidden rounded border-0 shadow-lg">
								<div class="card-body p-0">
									<img src="/frontend/images/home_about_02.png" class="img-fluid" alt="Scholar keys work online">
									<div class="overlay-work bg-primary"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-12 mt-4 mt-lg-0 pt-3 pt-lg-0">
					<div class="ml-lg-4">
						<div class="section-title mb-4 pb-2">
							<h4 class="title mb-0">About <span class="text-primary">ScholarKeys</span></h4>
							<p class="text-muted para-desc mx-auto mb-0">A little about ScholarKeys and what we <span class="text-primary font-weight-bold">plan to do</span></p>
						</div>
						<p class="text-justify">In today’s tug of war between <strong>School ERP software</strong>, we offer a solution that not only manages your school but also adds on to a quality of interactive learning opportunity to the future of our Country… and that is your students.</p>
						<p class="text-justify"><strong class="text-secondary">ScholarKeys</strong> itself is a story behind our mission. Provide a platform for higher learning. Yes, we aim to deliver that key to Scholars not just by selling the software that manages your schooling transaction but to provide a platform that will reshape the education system for productive learning.</p>
						<p>But... can software do that? <strong>As it turns out, we just did</strong>.</p>
						<div class="watch-video mt-4 pt-2">
							<a href="about.php" class="btn btn-primary">Read More <i class="mdi mdi-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container mt-100 mt-60">
			<div class="section-title mb-4 pb-2">
				<h4 class="title mb-0 text-center"><span class="text-primary">Who</span> is this for?</h4>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-3 col-lg-4 col-md-6 col-12">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_student.svg" style="width: 40px;" alt="Scholar keys student">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Learners</h5>
							<p class="text-muted mb-0 lh-1-2">Students get to learn with full productivity</p>
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-md-0 pt-2 pt-sm-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_teacher.svg" style="width: 40px;" alt="Scholar keys teacher">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Teachers</h5>
							<p class="text-muted mb-0 lh-1-2">Teaching has never been so easy</p>
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-lg-0 pt-2 pt-lg-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_parents.svg" style="width: 40px;" alt="Scholar keys parents">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Parents</h5>
							<p class="text-muted mb-0 lh-1-2">Stay updated with your child’s performance</p>
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-lg-5 mt-xl-0 pt-2 pt-lg-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_admin.svg" style="width: 40px;" alt="Scholar keys online">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Admins</h5>
							<p class="text-muted mb-0 lh-1-2">Manage your entire educational institutions</p>
						</div>
					</div>
				</div>

				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-lg-5 pt-2 pt-lg-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_accountant.svg" style="width: 40px;" alt="Scholar keys accountants">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Accountants</h5>
							<p class="text-muted mb-0 lh-1-2">Manage your financial transaction with ease</p>
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-lg-5 pt-2 pt-sm-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_librarian.svg" style="width: 40px;" alt="Scholar keys librarians">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Librarians</h5>
							<p class="text-muted mb-0 lh-1-2">Be the book guardian you are meant to be</p>
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-lg-5 pt-2 pt-lg-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_receptionist.svg" style="width: 40px;" alt="Scholar keys receptionists">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Receptionists</h5>
							<p class="text-muted mb-0 lh-1-2">Managing the invoice is easy</p>
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 mt-4 mt-lg-5 pt-2 pt-lg-0">
					<div class="media align-items-center features">
						<div class="icons ico-homepage m-0 rounded h2 text-secondary text-center px-2">
							<img src="/frontend/images/icon/home_custodian.svg" style="width: 40px;" alt="Scholar keys custodian">
						</div>
						<div class="content ml-4">
							<h5 class="mb-1">Custodian</h5>
							<p class="text-muted mb-0 lh-1-2">Stay up to date with your school's maintenance</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container mt-100 mt-60 text-center">
			<div class="section-title mb-4 pb-2">
				<h4 class="title mb-0"><span class="text-primary">Why</span> ScholarKeys?</h4>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-8 col-md-10 col-12">
					<p class="text-justify" style="text-align-last: center;">With multifunctional modules available for innovative learning, <strong>ScholarKeys</strong> provides an amazing platform which bridges the gaps between Learners, Educators and Parents for better learning experience. <strong>ScholarKeys</strong> will help you manage your educational institutes with our futuristic modules and game changing approach for education industry. Unlike others, <strong>ScholarKeys</strong> facilitates on providing an efficient management and productive schooling.</p>
					<p class="lead">Make learning and teaching easy to everyone with <strong>ScholarKeys</strong>.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="section-title mb-4 pb-2">
						<h4 class="title mb-0">Our Modules</h4>
						<p class="text-muted para-desc mx-auto mb-0">ScholarKeys is <span class="text-primary font-weight-bold">feature-packed</span> and covers all the services a school might need</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-12 mt-5">
					<div class="features text-center">
						<div class="image position-relative d-inline-block">
							<img src="/frontend/images/icon/dashboard.svg" class="avatar avatar-small" alt="Scholar keys dashoboard">
						</div>

						<div class="content mt-4">
							<h4 class="title-2">Custom-Built Dashboard</h4>
							<p class="text-muted mb-0 text-justify" style="text-align-last: center;">Why limit what you can see on your dashboard? You can choose what you want to see and focus on what is important to you. gone are those days where Software determines your limit.</p>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-12 mt-5">
					<div class="features text-center">
						<div class="image position-relative d-inline-block">
							<img src="/frontend/images/icon/notice.svg" class="avatar avatar-small" alt="Scholar keys noticeboard">
						</div>

						<div class="content mt-4">
							<h4 class="title-2">Notice Board</h4>
							<p class="text-muted mb-0 text-justify" style="text-align-last: center;">Share your news with your teachers and students with our Notice Board module where you can update and publish all the educational institute news to your users.</p>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-12 mt-5">
					<div class="features text-center">
						<div class="image position-relative d-inline-block">
							<img src="/frontend/images/icon/class.svg" class="avatar avatar-small" alt="Scholar keys virtual class">
						</div>

						<div class="content mt-4">
							<h4 class="title-2">Virtual Class</h4>
							<p class="text-muted mb-0 text-justify" style="text-align-last: center;">With our virtual class management system, arrange and schedule a live class with your students where learning can be both effective and productive.</p>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-12 mt-5">
					<div class="features text-center">
						<div class="image position-relative d-inline-block">
							<img src="/frontend/images/icon/routine.svg" class="avatar avatar-small" alt="Scholar keys routine">
						</div>

						<div class="content mt-4">
							<h4 class="title-2">Routine Management System</h4>
							<p class="text-muted mb-0 text-justify" style="text-align-last: center;">Manage the daily school routine of class time table with ease & allocate the subjects to the teachers effortlessly. Class routine is easily accessible to all the teachers.</p>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-12 mt-5">
					<div class="features text-center">
						<div class="image position-relative d-inline-block">
							<img src="/frontend/images/icon/chat.svg" class="avatar avatar-small" alt="Scholar keys chat">
						</div>

						<div class="content mt-4">
							<h4 class="title-2">In Chat module</h4>
							<p class="text-muted mb-0 text-justify" style="text-align-last: center;">ScholarKeys offers platform to communicate within employees with ease. Not just one to one but also group messaging has been made easy with our instant messaging system.</p>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-12 mt-5">
					<div class="features text-center">
						<div class="image position-relative d-inline-block">
							<img src="/frontend/images/icon/batch.svg" class="avatar avatar-small" alt="Scholar keys batch">
						</div>

						<div class="content mt-4">
							<h4 class="title-2">Batch Management</h4>
							<p class="text-muted mb-0 text-justify" style="text-align-last: center;">ScholarKeys presents a platform to manage the batch of entire institute & assign class teachers based on privileges. Class teachers can easily assign roll numbers to their students.</p>
						</div>
					</div>
				</div>
			</div>

			<p class="text-center mt-5 mb-0 pt-2"><a href="modules.php" class="btn btn-primary">View All Modules <i class="mdi mdi-chevron-right"></i></a></p>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="section-title mb-4 pb-2">
						<h4 class="title mb-0"><span class="text-primary">Special</span> Features</h4>
						<p class="text-muted para-desc mb-0 mx-auto">How ScholarKeys <span class="text-primary font-weight-bold">stands out</span> from the rest of the competition</p>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-8 col-md-12 mt-4 pt-2 text-center">
					<ul class="nav nav-pills nav-justified flex-column flex-sm-row rounded" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link rounded active" data-toggle="pill" href="#pills-curriculum" role="tab" aria-controls="pills-curriculum" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Curriculum</h4>
								</div>
							</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link rounded" data-toggle="pill" href="#pills-library" role="tab" aria-controls="pills-library" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Virtual Library</h4>
								</div>
							</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link rounded" data-toggle="pill" href="#pills-classroom" role="tab" aria-controls="pills-classroom" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Live Classroom</h4>
								</div>
							</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link rounded" data-toggle="pill" href="#pills-support" role="tab" aria-controls="pills-support" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Live Support</h4>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="col-12 mt-4 pt-2">
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-curriculum" role="tabpanel" aria-labelledby="pills-curriculum">
							<div class="row align-items-center">
								<div class="col-md-6">
									<img src="/frontend/images/curriculum.png" class="img-fluid mx-auto d-block rounded" alt="Scholar keys learner">
								</div>

								<div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
									<div class="section-title">
										<h4 class="title mb-3"><i class="mdi mdi-chevron-double-right text-primary mr-1"></i>Curriculum</h4>
										<p class="text-muted text-justify">Buckle up learners and educators as ScholarKeys brings you a Curriculum module where we want our nation to be part of a revolutionized learning platform. Yes, you heard it right. We ScholarKeys are coming up with an online Curriculum platform where teachers as well as students benefit greatly from what we like to call a game changing education system.</p>
										<ul class="list-unstyled feature-list text-muted">
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Teacher handbook(study guide curriculum)</li>
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Assessment curriculum</li>
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Evaluation curriculum</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
						<div class="tab-pane fade" id="pills-library" role="tabpanel" aria-labelledby="pills-library">
							<div class="row align-items-center">
								<div class="col-md-6">
									<img src="/frontend/images/library.png" class="img-fluid mx-auto d-block rounded" alt="Scholar keys library">
								</div>

								<div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
									<div class="section-title">
										<h4 class="title mb-3"><i class="mdi mdi-chevron-double-right text-primary mr-1"></i>Virtual library</h4>
										<p class="text-muted text-justify">Now let's talk Books. Nobody wants to visit a library and get a dusted book to read. That was a long, long time ago. How about going over to an internet and downloading the book you want? You like that, don’t you? Good… cause ScholarKeys brings you a Virtual Library module where people who love reading books can visit and download directly on their computing device. Read your favorite books, anywhere, anytime. Because there are more treasures in books than in all the pirate’s loot.</p>
										<ul class="list-unstyled feature-list text-muted">
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Download eBook anywhere</li>
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Choose from hundreds of topic to read</li>
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Be a part of our eLearning community</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade" id="pills-classroom" role="tabpanel" aria-labelledby="pills-classroom">
							<div class="row align-items-center">
								<div class="col-md-6">
									<img src="/frontend/images/live_classroom.png" class="img-fluid mx-auto d-block rounded" alt="Scholar keys live">
								</div>

								<div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
									<div class="section-title">
										<h4 class="title mb-3"><i class="mdi mdi-chevron-double-right text-primary mr-1"></i>Live Classroom</h4>
										<p class="text-muted text-justify">Yes, it is just like a regular classroom, everything remains the way it is. The only difference is: a Live classroom uses technology to support instruction and learning. What are the advantages of LIve classrooms? It offers more flexibility over a traditional classroom. Both the educators and learners have rich experience of collaboration. Now experience a new way of learning and educating with ScholarKeys.</p>
										<ul class="list-unstyled feature-list text-muted">
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Scheduled live classes as per school routine and curriculum</li>
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>Interactive sessions and two-way communication</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade" id="pills-support" role="tabpanel" aria-labelledby="pills-support">
							<div class="row align-items-center">
								<div class="col-md-6">
									<img src="/frontend/images/live_support.png" class="img-fluid mx-auto d-block rounded" alt="Scholar keys live support">
								</div>

								<div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
									<div class="section-title">
										<h4 class="title mb-3"><i class="mdi mdi-chevron-double-right text-primary mr-1"></i>Live Support</h4>
										<p class="text-muted text-justify">Behind this great innovative e-learning platform stands a great team of experts who are always ready to help you when you run into any problems. We are here to solve your query. Reach out to us on information below. teract with your students directly and start communicating. Do you want to IM parents too? Well, ScholarKeys does that too.</p>
										<ul class="list-unstyled feature-list text-muted">
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>support@scholarkeys.com</li>
											<li><i data-feather="check-circle" class="fea icon-sm text-success mr-2"></i>+977-9802320805</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container mt-100 mt-60">
			<div class="rounded bg-secondary p-lg-5 p-4">
				<div class="section-title text-center">
					<h2 class="text-white mb-0">See it to believe it.</h2>
					<h5 class="text-primary mb-3">Start your free trial today.</h5>
					<p class="text-white w-75 mx-auto mb-5">Be one of our early adopters and start running your school digitally with ScholarKeys.</p>
					<p><a href="demo.php" class="btn btn-primary">Try Our Free Demo <i class="mdi mdi-arrow-right"></i></a></p>
				</div>
			</div>
		</div>
	</section>
@endsection
