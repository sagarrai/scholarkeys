<header id="topnav" class="defaultscroll sticky">
	<div class="container-fluid">
		<a class="logo" href="/"><img src="/frontend/images/scholarkeys-logo-gold.png" alt=""></a>
		<div class="buy-button">
			<a class="btn btn-outline-primary px-3" href="quote">Get Quote</a>
			<a class="btn btn-primary ml-2 px-3" href="../demo">Free Demo</a>
		</div>

		<div class="menu-extras">
			<div class="menu-item">
				<a class="navbar-toggle">
					<div class="lines">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</a>
			</div>
		</div>

		<div id="navigation">
			<ul class="navigation-menu">
				<li><a href="/">Home</a></li>
				<li><a href="modules">Modules</a></li>
				<li><a href="curriculum">Curriculum</a></li>
				<li><a href="survey">Survey</a></li>

				<li class="has-submenu">
					<a href="javascript:void(0)">Our Company</a><span class="menu-arrow"></span>
					<ul class="submenu">
						<li><a href="about">About</a></li>
						<li><a href="careers">Careers</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</li>

				<li><a href="library"><span class="text-primary border border-primary-light rounded-sm px-2 py-1">Library</span></a></li>
			</ul>
		</div>
	</div>
</header>

<div id="preloader">
	<div class="logo-top-logo">
		<img src="/frontend/images/preloader-top-logo.png" alt="">
	</div>
	<div class="logo-btm-logo">
		<img src="/frontend/images/preloader-btm-logo.png" alt="">
	</div>
</div>