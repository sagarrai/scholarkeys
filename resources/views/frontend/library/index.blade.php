
<!DOCTYPE html>
<html lang="en">
<head>
	<title>ScholarKeys - Global Virtual Library</title>
	<meta name="description" content="ScholarKeys description goes here" />
	<meta name="keywords" content="ScholaryKeys, LMS, E-Learning, elearning" />
	
    @include('frontend.modules.header-script')

</head>
<body>
	
    @include('frontend.modules.header')

	<section class="section is-page-intro">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="title-heading mt-4">
						<h1 class="heading mb-0">Global Virtual <span class="text-primary">Library</span></h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
					<div class="position-relative">
						<img src="/frontend/images/library_1.jpg" class="rounded img-fluid mx-auto d-block" alt="">
					</div>
				</div>

				<div class="col-lg-7 col-md-7 mt-4 pt-2 mt-md-0 pt-sm-0">
					<div class="section-title ml-lg-4">
						<p class="lead">We have always imagined that Paradise will be a kind of a Library.</p>
						<p class="text-muted para-desc mx-auto mb-0 text-justify">ScholarKeys brings you a Global Virtual Library where you can get access to all kinds of reading materials online in one platform. With technology taking a huge part in changing all kinds of business, we can only imagine a day when reading a book can be as easy as reading an article online. Global Virtual Library offers you a wide range of book collection in different topics that you might like. Now you can directly sign up, start surfing and download your favorite book and start reading.</p>
					</div>
				</div>
			</div>

			<div class="row align-items-center mt-5">
				<div class="col-lg-7 col-md-7 mt-4 pt-2 mt-md-0 pt-sm-0 order-2 order-md-1">
					<div class="section-title ml-lg-4">
						<h4 class="title mb-3 mb-md-4">Why <span class="text-primary"> Global Virtual Library</span>?</h4>
						<p class="text-muted para-desc mx-auto text-justify">Global Virtual Library offers you an easy accessible platform for easy learning. Now your education institutes can directly upload course books for your students and help them get access remotely. Not only that, ScholarKeys is providing books from many different platforms in one module so our children can expand their book learning habit. With an intuitive User Interface, students can easily explore their interest, download and start reading.</p>
					</div>
				</div>

				<div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0 order-1 order-md-2">
					<div class="position-relative">
						<img src="/frontend/images/library_2.jpg" class="rounded img-fluid mx-auto d-block" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="container">
			<h4 class="title mb-0 text-center">We are undergoing <span class="text-primary">GVL setup</span>, stay tuned because it’s coming soon!</h4>
		</div>
	</section>

    @include('frontend.modules.footer')
	
</body>
</html>