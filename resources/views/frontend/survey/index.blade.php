
<!DOCTYPE html>
<html lang="en">
<head>
	<title>ScholarKeys - Take a Survey</title>
	<meta name="description" content="ScholarKeys LMS & ERP Solution, please take a survey" />
	<meta name="keywords" content="ScholaryKeys, LMS, E-Learning, elearning" />
	
    @include('frontend.modules.header-script')
	
</head>
<body>
	
    @include('frontend.modules.header')

	<section class="section is-page-intro">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<div class="title-heading mt-4">
						<h1 class="heading">ScholarKeys <span class="text-primary">Surveys</span></h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section bg-light">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
					<div class="position-relative">
						<img src="/frontend/images/survey.jpg" class="rounded img-fluid mx-auto d-block" alt="">
					</div>
				</div>
				<div class="col-lg-5 col-md-7 mt-4 pt-2 mt-md-0 pt-sm-0">
					<div class="section-title ml-lg-4">
						<p class="text-muted para-desc mx-auto mb-0 text-justify">Greetings from ScholarKeys, we have been analyzing the negative impact the education system had to bear during this COVID – 19 Pandemic which has disrupted our children’s academic year. To better analyze the problem we have come up with this survey.</p>
						<p class="text-muted para-desc mx-auto mb-0 text-justify">This survey should only take 5-7 minutes to complete. Please be assured that all the answers you provide will remain anonymous and will be used only for research purposes. Thank you for your time!</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section border-bottom">
		<div class="container">
			<div class="section-title">
				<h4 class="title mb-5 pb-2 text-center">Choose your <span class="text-primary">Survey</span></h4>
			</div>

			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11">
					<ul class="nav nav-pills nav-justified flex-column flex-sm-row rounded mb-4" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link rounded active" data-toggle="pill" href="#pills-student" role="tab" aria-controls="pills-student" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Student's Survey</h4>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link rounded" data-toggle="pill" href="#pills-teacher" role="tab" aria-controls="pills-teacher" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Teacher's Survey</h4>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link rounded" data-toggle="pill" href="#pills-parent" role="tab" aria-controls="pills-parent" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">Parent's Survey</h4>
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link rounded" data-toggle="pill" href="#pills-admin" role="tab" aria-controls="pills-admin" aria-selected="false">
								<div class="text-center pt-1 pb-1">
									<h4 class="title font-weight-normal mb-0">School Admin Survey</h4>
								</div>
							</a>
						</li>
					</ul>

					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-student" role="tabpanel" aria-labelledby="pills-student">
							<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfg32etUa5PQEyFj-T1IEy8fCBxebhOeq2XrIJJWat2g6UsoA/viewform?embedded=true" width="100%" height="560" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
						</div>
						
						<div class="tab-pane fade" id="pills-teacher" role="tabpanel" aria-labelledby="pills-teacher">
							<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSc-a58qiEprQk3rrOmANjqDz0KEl-4UjrgfCgm_nSWlTElBYQ/viewform?embedded=true" width="100%" height="560" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
						</div>

						<div class="tab-pane fade" id="pills-parent" role="tabpanel" aria-labelledby="pills-parent">
							<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSf1BgZlQfhRn5kKpGd4H7ANhDYYI4SYzL5bQUstAi-rvlKL1A/viewform?embedded=true" width="100%" height="560" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
						</div>

						<div class="tab-pane fade" id="pills-admin" role="tabpanel" aria-labelledby="pills-admin">
							<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdydxVpApPczgfMUffMT6DLnBOPGrKzsBIXU6E9-KIE_e5RgQ/viewform?embedded=true" width="100%" height="560" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    @include('frontend.modules.footer')
	
</body>
</html>