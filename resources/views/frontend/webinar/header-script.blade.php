<meta charset="utf-8" />
<meta property="og:image" content="/frontend/images/smm_thumbnail.png">
<meta property="og:image:secure_url" content="/frontend/images/smm_thumbnail.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="/frontend/images/favicon.png">
<link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="/frontend/css/materialdesignicons.min.css">
<link rel="stylesheet" href="/frontend/css/owl.carousel.min.css"> 
<link rel="stylesheet" href="/frontend/css/owl.theme.default.min.css"> 
<link rel="stylesheet" href="/frontend/css/main.css">