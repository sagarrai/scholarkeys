<div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="/admin-resources/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">Scholarkeys Admin</a>
        </div>
    </div>
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-user-md"></i>
                <p>
                    Career
                    <i class="fas fa-angle-left right"></i>
                    {{-- <span class="badge badge-info right">6</span> --}}
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('admin.career.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Career Lists</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.career.store') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add new Career</p>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-window-restore"></i>
                <p>
                    Demo
                    <i class="fas fa-angle-left right"></i>
                    {{-- <span class="badge badge-info right">6</span> --}}
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('admin.demo.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Demo Lists</p>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item has-treeview">
            <a href="{{ route('admin.job.index') }}" class="nav-link">
                <i class="nav-icon fas fa-briefcase"></i>
                <p>
                    Job Applicant List
                    {{-- <span class="badge badge-info right">6</span> --}}
                </p>
            </a>
        </li>
    </ul>
    </nav>
</div>