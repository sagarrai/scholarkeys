<!DOCTYPE html>
<html lang="en">
	<head>
		<title>ScholarKeys - Featured packed easy to use Learning Management System</title>
		<meta name="description" content="ScholarKeys description goes here" />
		<meta name="keywords" content="ScholaryKeys, LMS, E-Learning, elearning" />
		<meta charset="utf-8" />
		<meta property="og:image" content="/frontend/images/smm_thumbnail.png">
		<meta property="og:image:secure_url" content="/frontend/images/smm_thumbnail.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="/frontend/images/favicon.png">
		<link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
		<link rel="stylesheet" href="/frontend/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="/frontend/css/owl.carousel.min.css">
		<link rel="stylesheet" href="/frontend/css/owl.theme.default.min.css">
		<link rel="stylesheet" href="/frontend/css/main.css">
	</head>
	<body>
		<div id="preloader">
			<div class="logo-top-logo">
				<img src="/frontend/images/preloader-top-logo.png" alt="">
			</div>
			<div class="logo-btm-logo">
				<img src="/frontend/images/preloader-btm-logo.png" alt="">
			</div>
		</div>
		<header id="topnav" class="defaultscroll sticky">
			<div class="container-fluid">
				<a class="logo" href="/"><img src="/frontend/images/scholarkeys-logo-gold.png" alt=""></a>
				<div class="buy-button">
					<a class="btn btn-outline-primary px-3" href="quote">Get Quote</a>
					<a class="btn btn-primary ml-2 px-3" href="demo">Live Demo</a>
				</div>

				<div class="menu-extras">
					<div class="menu-item">
						<a class="navbar-toggle">
							<div class="lines">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</a>
					</div>
				</div>

				<div id="navigation">
					<ul class="navigation-menu">
						<li><a href="/">Home</a></li>
						<li><a href="modules">Modules</a></li>
						<li><a href="curriculum">Curriculum</a></li>
						<li><a href="survey">Survey</a></li>
						<li><a href="webinar">Webinar</a></li>

						<li class="has-submenu">
							<a href="javascript:void(0)">Our Company</a><span class="menu-arrow"></span>
							<ul class="submenu">
								<li><a href="about">About</a></li>
								<li><a href="careers">Careers</a></li>
								<li><a href="contact">Contact</a></li>
							</ul>
						</li>

						<li><a href="library/"><span class="text-primary border border-primary-light rounded-sm px-2 py-1">Library</span></a></li>
					</ul>
				</div>
			</div>
		</header>

		@yield('content')
		
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
						<a class="d-block" href="/"><img src="/frontend/images/scholarkeys-logo-gold.png" class="img-fluid w-75" alt=""></a>
						<p class="mt-4 small text-justify">Scholar Keys provides a perfect platform where students, teachers, and parents can interact with one another to make sure that they could receive incisive reports about each other’s performance.</p>
						<ul class="list-unstyled social-icon social mb-0 mt-4">
							<li class="list-inline-item"><a href="https://www.facebook.com/scholarkeysnepal" target="_blank" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
							<li class="list-inline-item"><a href="https://www.instagram.com/scholarkeys/" target="_blank" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
							<li class="list-inline-item"><a href="https://linkedin.com/company/scholarkeys/" target="_blank" class="rounded"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
						</ul>
					</div>
					<div class="col-lg-2 col-md-4 col-sm-6 col-12 mt-4 mt-md-0 pt-2 pt-md-0">
						<h4 class="text-light footer-head">Company</h4>
						<ul class="list-unstyled footer-list mt-4">
							<li><a href="about" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>About</a></li>
							<li><a href="modules" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Modules</a></li>
							<li><a href="quote" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Get Quote</a></li>
							<li><a href="demo" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Demo</a></li>
							<li><a href="contact" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Contact</a></li>
						</ul>
					</div>
					
					<div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-4 mt-md-0 pt-2 pt-md-0">
						<h4 class="text-light footer-head">Modules</h4>
						<ul class="list-unstyled footer-list mt-4">
							<li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Interactive Dashboard</a></li>
							<li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Classroom Schedule</a></li>
							<li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Information Circulation</a></li>
							<li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Transportation</a></li>
							<li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>Subject Dashboard</a></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-4 col-12 mt-4 mt-md-0 pt-2 pt-md-0">
						<h4 class="text-light footer-head">Contact Us</h4>
						<p class="mt-4">Have any queries? Let us know what you're thinking of.</p>
						<p><a href="contact" class="btn btn-primary btn-block">Contact Us</a></p>
					</div>
				</div>
			</div>
		</footer>
		<footer class="footer footer-bar">
			<div class="container d-flex flex-lg-row flex-column justify-content-lg-between justify-content-center text-lg-left text-center">
				<p class="text-white mb-0 py-2">A PGNSONS Company.</p>
				<p class="mb-0 py-2"> ScholarKeys © {{ date('Y') }}. Powered by Analogue Inc. All Rights Reserved. </p>
			</div>
		</footer>
		<a href="#" class="back-to-top rounded text-center" id="back-to-top">
			<i data-feather="chevron-up" class="icons d-inline-block"></i>
		</a>
		<script src="/frontend/js/jquery-3.4.1.min.js"></script>
		<script src="/frontend/js/bootstrap.bundle.min.js"></script>
		<script src="/frontend/js/jquery.easing.min.js"></script>
		<script src="/frontend/js/scrollspy.min.js"></script>
		<script src="/frontend/js/owl.carousel.min.js "></script>
		<script src="/frontend/js/owl.init.js "></script>
		<script src="/frontend/js/feather.min.js"></script>
		<script src="/frontend/js/unicons-monochrome.js"></script>
		<script src="/frontend/js/app.js"></script>
	</body>
</html>