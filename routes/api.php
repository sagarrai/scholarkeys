<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('allusers', 'API\UserController@allUsers');

Route::group(['middleware' => 'auth:api'], function(){
    Route::apiResource('career','API\CareerController');
    Route::post('career/create', 'API\CareerController@store');
    Route::apiResource('demo','API\DemoController');
    Route::apiResource('job','API\JobController');
});
   
Route::post('demo/create', 'API\DemoController@store');
Route::post('job/create', 'API\JobController@store');
