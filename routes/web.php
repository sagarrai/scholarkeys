<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//All Frontend Routes Starts

Route::get('/', 'HomeController@index')->name('homepage');
Route::get('/careers', 'HomeController@career')->name('careers');
Route::get('/career_details/{title}','HomeController@career_details')->name('career_details');
Route::get('/career_apply/{title}', 'HomeController@career_apply')->name('career_apply');

Route::get('/demo', 'HomeController@demo')->name('demo');
Route::get('/curriculum', 'HomeController@curriculum')->name('curriculum');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/quote', 'HomeController@quote')->name('quote');

Route::get('modules','HomeController@modules')->name('modules');
Route::get('survey','HomeController@survey')->name('survey');
Route::get('library','HomeController@library')->name('library');
Route::get('webinar','HomeController@webinar')->name('webinar');


//All Frontend Routes Ends

//Admin Routes Here

Route::prefix('admin')->name('admin.')->group(function(){
    Route::get('dashboard','Admin\DashboardController@index')->name('dashboard');

    Route::get("career/allcareers","Admin\CareerController@index")->name("career.index");
    Route::get("career","Admin\CareerController@store")->name("career.store");
    Route::get("career/edit/{id}","Admin\CareerController@edit")->name("career.edit");

    Route::get("demo/alldemos","Admin\DemoController@index")->name("demo.index");
    Route::get("demo","Admin\DemoController@store")->name("demo.store");
    Route::get("demo/edit/{id}","Admin\DemoController@edit")->name("demo.edit");

    Route::get("job/alljobs","Admin\JobController@index")->name("job.index");
    Route::get("job/{id}","Admin\JobController@show")->name("job.show");

});

Route::prefix('admin')->group(function(){
    Auth::routes();
});

Route::get('/home', 'HomeController@index')->name('home');
