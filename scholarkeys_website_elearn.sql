-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 14, 2020 at 08:44 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `scholarkeys`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_opening` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `experience_qualification` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `no_of_opening`, `description`, `experience_qualification`, `created_at`, `updated_at`) VALUES
(4, 'Senior Laravel Developer', 3, '<p>test</p>', '<p>test</p>', '2020-06-11 05:30:16', '2020-06-11 05:30:16'),
(5, 'PHP Developer', 2, '<p>test</p>', '<p>test</p>', '2020-06-11 21:57:37', '2020-06-11 21:57:37');

-- --------------------------------------------------------

--
-- Table structure for table `demos`
--

CREATE TABLE `demos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `demos`
--

INSERT INTO `demos` (`id`, `full_name`, `email`, `phone_number`, `school_name`, `remarks`, `created_at`, `updated_at`) VALUES
(2, 'Sagar Rai', 'sagar@gmail.com', '123456789', 'Analogue School', 'lorem ipsum', '2020-06-07 06:13:31', '2020-06-07 06:13:31'),
(7, 'Sagar Rai', 'sagar.raiii31@gmail.com', '9816308014', 'test', 'test', '2020-06-08 22:34:19', '2020-06-08 22:34:19'),
(13, 'Sagar Rai', 'sagar.raiii31@gmail.com', '9816308014', 'test', 'testtestsetstset', '2020-06-09 22:05:21', '2020-06-09 22:05:21'),
(14, 'Sagar Rai', 'sagar.raiii31@gmail.com', '9816308014', 'Analogue Inc', 'Testing my test hehe', '2020-06-13 11:13:22', '2020-06-13 11:13:22');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apply_for` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apply_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `full_name`, `phone_number`, `email_address`, `address_line`, `apply_for`, `expected_salary`, `apply_date`, `cv_file`, `cover_letter`, `created_at`, `updated_at`) VALUES
(3, 'Sagar Rai', '9816308014', 'sagar.raiii31@gmail.com', 'Nakhipot', NULL, '60000', '2020-06-11 11:06', 'sagar.raiii31@gmail.com863525502.PNG', 'sagar.raiii31@gmail.com991806318.jpg', '2020-06-11 05:31:17', '2020-06-11 05:31:17'),
(4, 'Sagar Rai', '9816308014', 'sagar.raiii31@gmail.com', 'Nakhipot', NULL, '123123', '2020-06-12 07:06', 'sagar.raiii31@gmail.com1582217809.jpg', 'sagar.raiii31@gmail.com365079709.JPG', '2020-06-12 01:31:27', '2020-06-12 01:31:27'),
(5, 'Sagar Rai', '9816308014', 'sagar.raiii31@gmail.com', 'Nakhipot', 'PHP Developer', '30000', '2020-06-12 07:06', 'sagar.raiii31@gmail.com247759978.jpg', 'sagar.raiii31@gmail.com1859374211.jpg', '2020-06-12 01:38:29', '2020-06-12 01:38:29'),
(6, 'Sagar Rai', '9816308014', 'sagar.raiii31@gmail.com', 'Nakhipot', NULL, '100000', '2020-06-12 07:06', 'sagar.raiii31@gmail.com58250751.jpg', 'sagar.raiii31@gmail.com463620840.JPG', '2020-06-12 01:39:11', '2020-06-12 01:39:11'),
(7, 'Sanam Sitaula', '9816308014', 'sagar.raiii31@gmail.com', 'Nakhipot', 'PHP Developer', '121313123', '2020-06-14 05:06', 'sagar.raiii31@gmail.com1344120803.JPG', 'sagar.raiii31@gmail.com134663187.jpg', '2020-06-13 23:59:34', '2020-06-13 23:59:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 2),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(11, '2020_06_03_103901_create_permission_tables', 4),
(12, '2020_06_04_093804_create_careers_table', 5),
(15, '2014_10_12_000000_create_users_table', 6),
(16, '2020_06_07_063638_create_demos_table', 7),
(20, '2020_06_09_060724_create_job_applieds_table', 8),
(21, '2020_06_11_054239_create_jobs_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00881424a3eee27e6506d3ab3989a8837909eb24592306f284ca35608ef8c685713dca074cec9448', 1, 1, 'MyApp', '[]', 0, '2020-06-04 07:57:53', '2020-06-04 07:57:53', '2021-06-04 13:42:53'),
('00bbcb178745cc9561b0731ed89746e70d09594b366ed9ae87c0b9f0d3aa998a584779c3f0a38844', 1, 1, 'MyApp', '[]', 0, '2020-06-05 02:58:27', '2020-06-05 02:58:27', '2021-06-05 08:43:27'),
('039dade53ee6c212090230b35f405b7b892a914858f06372cd85cf84723363c8b583cd41f5a921cb', 1, 1, 'MyApp', '[]', 0, '2020-06-13 01:29:49', '2020-06-13 01:29:49', '2021-06-13 07:14:49'),
('06dffabfc5656453e2f0ec6b1f0ede005ef9d2c92b246187e7251092301da28a7a12bbfefb8f89f1', 1, 1, 'MyApp', '[]', 0, '2020-06-04 05:23:06', '2020-06-04 05:23:06', '2021-06-04 11:08:06'),
('0ac6acea638ccb0f0d729e613f6937bdcb492dadf4c9bfb93745136eaf3abe0d1c78e567e57ee568', 1, 1, 'MyApp', '[]', 0, '2020-06-04 23:14:20', '2020-06-04 23:14:20', '2021-06-05 04:59:20'),
('0ad58f004233d56f29c442bf52d41a10db71ed5dd147c8b2ea661263d821db2482edf732217f2e2e', 1, 1, 'MyApp', '[]', 0, '2020-06-08 04:23:41', '2020-06-08 04:23:41', '2021-06-08 10:08:41'),
('1224b2b3fa8a1852661ff727153d36b09d8ad3b4cbfe1d8bafce298b298ff98a5b81ec3181cf8a65', 1, 1, 'MyApp', '[]', 0, '2020-06-09 22:05:49', '2020-06-09 22:05:49', '2021-06-10 03:50:49'),
('15ab7926ae230de6370b84c7c15728850a33865ee8a557af1a65825ee766a48e77b6511015df5392', 1, 1, 'MyApp', '[]', 0, '2020-06-11 06:16:03', '2020-06-11 06:16:03', '2021-06-11 12:01:03'),
('17a159caa36482fa34455dfb46a86cf89d326de27a4b0a386b06d49f39a277e4302e0237e5bd4c49', 1, 1, 'MyApp', '[]', 0, '2020-06-11 08:13:06', '2020-06-11 08:13:06', '2021-06-11 13:58:06'),
('1c2a9fb1cb557c10ca53b42757ae13ec1ff25d8b9cd0570e1b81884b38528987dc0b02212fac005b', 1, 1, 'MyApp', '[]', 0, '2020-06-05 03:45:14', '2020-06-05 03:45:14', '2021-06-05 09:30:14'),
('24a1ea7f53a33f35214ecdc5690ec000833be90f0862b4b2bed6add5caba238e89d1deaa8f9c873c', 1, 1, 'MyApp', '[]', 0, '2020-06-13 23:58:10', '2020-06-13 23:58:10', '2021-06-14 05:43:10'),
('2c1a9bb1d7c0563a8694d335fd2343f01fb459e91d0ab9ba337b7f4804895850152a1da8cc30c6dd', 1, 1, 'MyApp', '[]', 0, '2020-06-04 03:28:13', '2020-06-04 03:28:13', '2021-06-04 09:13:13'),
('2efac01d90b3f4067382d09ef455570ca32bc31e19e46b09dae6f7c39e1fe45e639d9867ebf05664', 1, 1, 'MyApp', '[]', 0, '2020-06-09 00:13:40', '2020-06-09 00:13:40', '2021-06-09 05:58:40'),
('2fd39019ad4c21c624466de3c8216bbfac46dae5a30620edba918aeeda74d15ddf9df596269cd781', 1, 1, 'MyApp', '[]', 0, '2020-06-11 09:14:12', '2020-06-11 09:14:12', '2021-06-11 14:59:12'),
('30f9a986f13b6eb1388bd7d2d075545d062bcfb24d8d8ca4a2eb208c70bc5b90bc0107909aca6b5c', 1, 1, 'MyApp', '[]', 0, '2020-06-05 05:40:21', '2020-06-05 05:40:21', '2021-06-05 11:25:21'),
('3597c1ff8ffef878b3416a105f91c6a4d58b1caced3f83bb43701dd2d2e7fe46646e010451d48b29', 1, 1, 'MyApp', '[]', 0, '2020-06-03 04:54:58', '2020-06-03 04:54:58', '2021-06-03 10:39:58'),
('3d53dc9b975dfdc5aadf36b6b59c5725414c0354151ab84f8d1d5e3db9031e58af80bf4ac2255d8b', 1, 1, 'MyApp', '[]', 0, '2020-06-11 07:56:15', '2020-06-11 07:56:15', '2021-06-11 13:41:15'),
('3d61991409af2edd5f8fc6fade4feb0b5a163f5d1906f7dda9652369d67519c75154e62382073f1a', 1, 1, 'MyApp', '[]', 0, '2020-06-05 07:15:20', '2020-06-05 07:15:20', '2021-06-05 13:00:20'),
('43c8a32d28c4bbebebdfb14e7bb80efa810704f09337342602e6319dcc33967738b31e93a239c0a7', 1, 1, 'MyApp', '[]', 0, '2020-06-06 10:03:45', '2020-06-06 10:03:45', '2021-06-06 15:48:45'),
('51bc1f75542b07f1f7e8c3af7d61877a888833eaa095cb4bb7d71d99b76b0e2d1cbde865d802aec0', 1, 1, 'MyApp', '[]', 0, '2020-06-04 23:52:02', '2020-06-04 23:52:02', '2021-06-05 05:37:02'),
('5867fba87eff869da455a896cff6b6156343bfb95f889ef2001a02e5f88ab416de595264a2405d17', 1, 1, 'MyApp', '[]', 0, '2020-06-04 07:58:37', '2020-06-04 07:58:37', '2021-06-04 13:43:37'),
('590c460274371f008ec460483c152b635ef7940fa4c4dbb5cb750dd01bb5b73af41da1ccb2448dd5', 1, 1, 'MyApp', '[]', 0, '2020-06-04 21:44:05', '2020-06-04 21:44:05', '2021-06-05 03:29:05'),
('5da9e825cc13a72b1d7ecfa6ad1d3068a3fde1d1e34d10ebcdd1c0226907a84df1b3ece54d02a3c5', 1, 1, 'MyApp', '[]', 0, '2020-06-04 22:20:57', '2020-06-04 22:20:57', '2021-06-05 04:05:57'),
('5e0b86155f1ba3c178d4c2343df5da8ad9304483a144eeefa9e33828214ee03a340bfea8e13d51f0', 1, 1, 'MyApp', '[]', 0, '2020-06-08 22:25:40', '2020-06-08 22:25:40', '2021-06-09 04:10:40'),
('5ff17415e08949cade333577627b1b130008d25229547876912ae4ac6b42a06082335923f4254e17', 1, 1, 'MyApp', '[]', 0, '2020-06-07 03:49:45', '2020-06-07 03:49:45', '2021-06-07 09:34:45'),
('60bde0e5a5e16a49171276fb15d3074189f7dda7bf8ad777ffdfe7c4990174b25d49207c7154717f', 1, 1, 'MyApp', '[]', 0, '2020-06-09 09:50:48', '2020-06-09 09:50:48', '2021-06-09 15:35:48'),
('64086851b6daa3a0716d7d40ada2d3c78789037fd08c45283cdd829865c8a7d71face955b544034b', 1, 1, 'MyApp', '[]', 0, '2020-06-05 03:44:10', '2020-06-05 03:44:10', '2021-06-05 09:29:10'),
('65776cc7f189eb82ef6b4d0f8afc93befdbf28c604ebf73adb1e5c89777ad8c5041b8452c46a0644', 1, 1, 'MyApp', '[]', 0, '2020-06-07 03:45:17', '2020-06-07 03:45:17', '2021-06-07 09:30:17'),
('6c32530d52575346f7085029fa75503b01fb63a31f059626cc84f1c75a04e1f8e34cdc78dd48ef72', 1, 1, 'MyApp', '[]', 0, '2020-06-13 11:50:51', '2020-06-13 11:50:51', '2021-06-13 17:35:51'),
('6fe63207afbb9071d843fe021cca8895d0dbdb149b6a5a22fcb839836d0f34bfdefa94fc000901de', 1, 1, 'MyApp', '[]', 0, '2020-06-04 21:54:53', '2020-06-04 21:54:53', '2021-06-05 03:39:53'),
('74d1f0458e2d61cfce3f5416202e17f2c6261c0647fd50efe924d429d86ceae1c260a4ca7e79346b', 1, 1, 'MyApp', '[]', 0, '2020-06-04 05:19:16', '2020-06-04 05:19:16', '2021-06-04 11:04:16'),
('74d3b6cd62dc0e7a170365d2cb24c6331b09436fd064a144f6d5aa30c11248e89e121f89bee28cfe', 1, 1, 'MyApp', '[]', 0, '2020-06-04 08:00:27', '2020-06-04 08:00:27', '2021-06-04 13:45:27'),
('78b7adf765ac11724b2c9acaa5e53bde84497b3bcacf53c473c03951d517979ddf96a3ea89cae08c', 1, 1, 'MyApp', '[]', 0, '2020-06-05 01:28:14', '2020-06-05 01:28:14', '2021-06-05 07:13:14'),
('79b067ee2037ea0eded1255db9158cb7e5d36594e2b8e3ace365ee1591f6a81aeda25aee9c1f538c', 1, 1, 'MyApp', '[]', 0, '2020-06-04 03:05:29', '2020-06-04 03:05:29', '2021-06-04 08:50:29'),
('7a112ddf6fb9d2165160e224826781eb18263c1255598d1b1ade7a3e3413c2d8e059b5e576665ee1', 1, 1, 'MyApp', '[]', 0, '2020-06-05 05:41:39', '2020-06-05 05:41:39', '2021-06-05 11:26:39'),
('7cb12b2553cbb44136f0016a98ec56aa6e9659911e0f2c2e842166ce8dc3ba17762c006df7c53939', 1, 1, 'MyApp', '[]', 0, '2020-06-04 07:44:13', '2020-06-04 07:44:13', '2021-06-04 13:29:13'),
('8b1ad72859ea880833c6e6b5c0631f7c502599b0873bb969262a6f4510ea4749d3d3c5cc8f4bfc79', 1, 1, 'MyApp', '[]', 0, '2020-06-11 07:56:15', '2020-06-11 07:56:15', '2021-06-11 13:41:15'),
('922f5d8feb13aa7a91a87e541df522c111420d3499cb9249bc2f541185043f16514b1123edb62da0', 1, 1, 'MyApp', '[]', 0, '2020-06-04 21:54:46', '2020-06-04 21:54:46', '2021-06-05 03:39:46'),
('94d6931d14a7001c670ab42b48dde76820a09faa5eb10e33741cdbc3827f6c121b9464d3227bebcc', 1, 1, 'MyApp', '[]', 0, '2020-06-05 02:40:05', '2020-06-05 02:40:05', '2021-06-05 08:25:05'),
('a3191222c8d85b20ae88825ff466d5e82800e769b37ff3444d5bf1cc478a510809f13f674cdf71e9', 1, 1, 'MyApp', '[]', 0, '2020-06-07 03:56:01', '2020-06-07 03:56:01', '2021-06-07 09:41:01'),
('a4343416875833ccd05a400abba4b830ec51aa1963fba873838609ad2389871b08572d2ab3c89ce1', 1, 1, 'MyApp', '[]', 0, '2020-06-11 08:13:14', '2020-06-11 08:13:14', '2021-06-11 13:58:14'),
('ab16b4edb06816b7410aa7eb21e4716c6c6bd21940cd2e828ae65f85ea0413b012e2734b52d4b632', 1, 1, 'MyApp', '[]', 0, '2020-06-11 04:44:30', '2020-06-11 04:44:30', '2021-06-11 10:29:30'),
('b6a5ff60f3d3ca6161c0519b11b3951c50a8fa5672ab6cd9f96fd507791d7c085f49319bf0bd158f', 1, 1, 'MyApp', '[]', 0, '2020-06-05 05:30:27', '2020-06-05 05:30:27', '2021-06-05 11:15:27'),
('b73e168e2555335edc350a7804e50f542490f60c3e58d7a2c9d7ada5b7cefdb188ae4deb75f2067d', 1, 1, 'MyApp', '[]', 0, '2020-06-08 21:48:15', '2020-06-08 21:48:15', '2021-06-09 03:33:15'),
('c19acef6c09a185d0123317decae78c147995fc9b63cdf39fde78c93686cf36a6f6ea7d842672962', 1, 1, 'MyApp', '[]', 0, '2020-06-07 21:54:29', '2020-06-07 21:54:29', '2021-06-08 03:39:29'),
('c451f1a9428d991be2f9a9997ff980a346e2ea41a395f0c40f83b3541619656d723349a0ac97ac97', 1, 1, 'MyApp', '[]', 0, '2020-06-04 22:00:12', '2020-06-04 22:00:12', '2021-06-05 03:45:12'),
('d07b16eaa8d0738dae1ba219ee80bb68661214e43a032119a887e9092be736fc388190a8ab143c3a', 1, 1, 'MyApp', '[]', 0, '2020-06-07 03:46:41', '2020-06-07 03:46:41', '2021-06-07 09:31:41'),
('d43e88841b2da51e955eca225cb961037e39241d3cc814049fc3b02ec0cf54821532d29df05deba8', 1, 1, 'MyApp', '[]', 0, '2020-06-08 06:00:54', '2020-06-08 06:00:54', '2021-06-08 11:45:54'),
('d59e080fc72985902cb81e141acb1f4246c8b108bfff997ef8d9552d9c18666ad43c23b3cfcb501a', 1, 1, 'MyApp', '[]', 0, '2020-06-09 05:48:16', '2020-06-09 05:48:16', '2021-06-09 11:33:16'),
('e1580275e53ff097707596fcfe0278cc2d142693317fbdadfa301f567e7c6fcbd584929f6b4baed0', 1, 1, 'MyApp', '[]', 0, '2020-06-05 06:23:58', '2020-06-05 06:23:58', '2021-06-05 12:08:58'),
('e1b37b8075625acebeb4b279d7d468d314c5ec421ba0aeb22dbe0216f47109664b562bca19e3a4a5', 1, 1, 'MyApp', '[]', 0, '2020-06-04 05:25:21', '2020-06-04 05:25:21', '2021-06-04 11:10:21'),
('e63c1d979f3dab910a3e658b7aa22e574817a2148c3f9e8d197b4ffa4616957f99738f7c76231cc5', 1, 1, 'MyApp', '[]', 0, '2020-06-04 05:24:23', '2020-06-04 05:24:23', '2021-06-04 11:09:23'),
('ef8a1b9cd95d9a15f6385b4ce0044466e0562eef80e14ae79123bd8adeb688f3b3739b09c9e4a6d9', 1, 1, 'MyApp', '[]', 0, '2020-06-04 21:54:37', '2020-06-04 21:54:37', '2021-06-05 03:39:37'),
('f098f8773ccb9014ebb1c1e84abdaceb45a064f298f3452822c607964ccf7b8bc2da5b2b549664d6', 1, 1, 'MyApp', '[]', 0, '2020-06-06 09:54:16', '2020-06-06 09:54:16', '2021-06-06 15:39:16'),
('fb10a4b58d3b6b4692152a1fbb4dd44035a121e8d4dab99349e845b7e684d8812630deed0b9b9c52', 1, 1, 'MyApp', '[]', 0, '2020-06-05 03:35:45', '2020-06-05 03:35:45', '2021-06-05 09:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '9EtCCfEWkaNBXWHhHuhvBSlcwanEMvHNsH8lvJu6', NULL, 'http://localhost', 1, 0, 0, '2020-06-03 01:41:01', '2020-06-03 01:41:01'),
(2, NULL, 'Laravel Password Grant Client', 'hvj7Uwdp7ZkTXBcU0GrZe6aWvtsO8fBvtOAquzk7', 'users', 'http://localhost', 0, 1, 0, '2020-06-03 01:41:01', '2020-06-03 01:41:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-03 01:41:01', '2020-06-03 01:41:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `status`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sagar Rai', 1, 'sagar@gmail.com', NULL, '$2y$10$zo3zp/bs0Hef.s0xg7vneOhdiDuT2r1je1H2zSL2AoWTW8JbxCv8S', NULL, '2020-06-04 07:57:53', '2020-06-04 07:57:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demos`
--
ALTER TABLE `demos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `demos`
--
ALTER TABLE `demos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
